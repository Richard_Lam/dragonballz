export class Timer {
    timer: number;
    threshold: number;
    constructor(threshold) {
        this.timer = 0;
        this.threshold = threshold;
    }
    update(executeFunction) {
        if (this.timer === this.threshold) {
            executeFunction();
            this.timer = 0;
        }
        else {
            this.timer++;
        }
    }
}