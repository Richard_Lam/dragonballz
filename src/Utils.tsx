
export const isRectsIntersect = (rect1: Polygon, rect2: Polygon) => {
    const [rect1_x1, rect1_y1, rect1_x2, rect1_y2] = rect1.getVertices();
    const [rect2_x1, rect2_y1, rect2_x2, rect2_y2] = rect2.getVertices();
    // If one rectangle is to the left of the other
    if (rect1_x2 < rect2_x1 || rect2_x2 < rect1_x1) {
        return false;
    }
    // If one rectangle is above other
    if (rect1_y2 < rect2_y1 || rect2_y2 < rect1_y1) {
        return false;
    }
    return true;
}

export const intersect = (rect1: number[], rect2: number[]) => {
    const [rect1_x1, rect1_y1, rect1_x2, rect1_y2] = [rect1[0], rect1[1], rect1[0] + rect1[2], rect1[1] + rect1[3]];
    const [rect2_x1, rect2_y1, rect2_x2, rect2_y2] = [rect2[0], rect2[1], rect2[0] + rect2[2], rect2[1] + rect2[3]];
    // If one rectangle is to the left of the other
    if (rect1_x2 < rect2_x1 || rect2_x2 < rect1_x1) {
        return false;
    }
    // If one rectangle is above other
    if (rect1_y2 < rect2_y1 || rect2_y2 < rect1_y1) {
        return false;
    }
    return true;
}

export class Polygon {
    x: number;
    y: number;
    width: number;
    height: number;
    constructor(list: number[]) {
        this.x = list[0];
        this.y = list[1];
        this.width = list[2];
        this.height = list[3];
    }
    getVertices() {
        return [this.x, this.y, this.x + this.width, this.y + this.height];
    }
}

export const minZero = (number: number) => {
    return number >= 0 ? number : 0;
}

const damageScaler = 0.7;

export const calculatePunchDamage = (attack: number, defense: number) => {
    const BASE_PUNCH_DAMAGE = 5;
    return damageScaler * (BASE_PUNCH_DAMAGE * attack / defense);
}

export const calculateKiBlastDamage = (kiBlastDamage: number, specialAttack: number, defense: number) => {
    return damageScaler * (kiBlastDamage * specialAttack / defense);
}

export const calculateSelfExplosionDamage = (damage: number, specialAttack: number, defense: number) => {
    return damageScaler * (1.1 * damage * specialAttack / defense);
}

export const calculateKiWaveDamage = (kiWaveDamage: number, specialAttack: number, defense: number) => {
    return damageScaler * (kiWaveDamage * specialAttack / defense);
}

export const calcBaseSpeed = (speed: number) => {
    // Range from 4 to 8
    return 4 + 4 * (speed / 1000);
}

export const getDistance = (points1, points2) => {
    return ((points1[0] - points2[0])**2 + (points1[1] - points2[1])**2)**0.5;
}

export const randomInt = (a: number, b: number) => {
    return a + Math.floor(Math.random()*(b-a+1));
}

export const getProfilePicPath = (imageName: string) => {
    return `./sprites/profile_pics/${imageName}_pfp.png`;
}
