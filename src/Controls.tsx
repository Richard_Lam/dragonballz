export interface actions {
    up: number;
    down: number;
    left: number;
    right: number;
    c: number;
    x: number;
    z: number;
}

export class Controls {
    
    controls: actions;

    constructor() {
        this.controls = {up: 0, down: 0, left: 0, right: 0, c: 0, x: 0, z: 0};
        this.addEventListeners();
    }
    getActions() {
        return this.controls;
    }
    addEventListeners() {
        document.body.addEventListener("keydown", (event) => {
            if (event.key === "c") {
                this.controls.c = 1;
                event.preventDefault();
            }
            else if (event.key === "x") {
                this.controls.x = 1;
                event.preventDefault();
            }
            else if (event.key === "z") {
                this.controls.z = 1;
                event.preventDefault();
            }
            switch (event.key) {
                case "w":
                    this.controls.up = 1;
                    event.preventDefault();
                    break;
                case "ArrowUp":
                    this.controls.up = 1;
                    event.preventDefault();
                    break;
                case "s":
                    this.controls.down = 1;
                    event.preventDefault();
                    break;
                case "ArrowDown":
                    this.controls.down = 1;
                    event.preventDefault();
                    break;
                default:
                    break;
            }
            switch (event.key) {
                case "a":
                    this.controls.left = 1;
                    event.preventDefault();
                    break;
                case "ArrowLeft":
                    this.controls.left = 1;
                    event.preventDefault();
                    break;
                case "d":
                    this.controls.right = 1;
                    event.preventDefault();
                    break;
                case "ArrowRight":
                    this.controls.right = 1;
                    event.preventDefault();
                    break;
                default:
                    break;
            }
        });
        document.body.addEventListener("keyup", (event) => {
            if (event.key === "c") {
                this.controls.c = 0;
            }
            else if (event.key === "x") {
                this.controls.x = 0;
            }
            else if (event.key === "z") {
                this.controls.z = 0;
            }
            switch (event.key) {
                case "w":
                    this.controls.up = 0;
                    break;
                case "ArrowUp":
                    this.controls.up = 0;
                    break;
                case "s":
                    this.controls.down = 0;
                    break;
                case "ArrowDown":
                    this.controls.down = 0;
                    break;
                default:
                    break;
            }
            switch (event.key) {
                case "a":
                    this.controls.left = 0;
                    break;
                case "ArrowLeft":
                    this.controls.left = 0;
                    break;
                case "d":
                    this.controls.right = 0;
                    break;
                case "ArrowRight":
                    this.controls.right = 0;
                    break;
                default:
                    break;
            }
        });
    }
}
