// @ts-ignore
import { Objects } from "./Objects.tsx";

const ExplosionsMapper = (damage: number) => {
    if (damage < 30) {
        return {width: 22.5, height: 23, image: document.getElementById("small_explosions"), frameDelay: 1};
    }
    if (damage < 70) {
        return {width: 45, height: 45, image: document.getElementById("medium_explosions"), frameDelay: 1};
    }
    if (damage < 100) {
        return {width: 67.5, height: 68, image: document.getElementById("large_explosions"), frameDelay: 1};
    }
    if (damage < 130) {
        return {width: 90, height: 90, image: document.getElementById("super_large_explosions"), frameDelay: 2};
    }
    if (damage < 170) {
        return {width: 120, height: 120, image: document.getElementById("giant_explosions"), frameDelay: 2};
    }
    return {width: 200, height: 200, image: document.getElementById("super_giant_explosions"), frameDelay: 3};
}

export class Explosions {

    x: number;
    y: number;
    width: number;
    height: number;
    counter: number;
    timer: number;
    image: HTMLElement | null;
    frameDelay: number;
    objects: Objects;

    constructor(x: number, y: number, objects: Objects, damage: number) {
        const explosionsMap = ExplosionsMapper(damage);
        [this.width, this.height, this.image, this.frameDelay] = [explosionsMap.width, explosionsMap.height, explosionsMap.image, explosionsMap.frameDelay];
        this.counter = 0;
        this.timer = 0;
        this.objects = objects;
        this.x = x - this.width / 2;
        this.y = y - this.height / 2;
        this.objects.addExplosion(this);
    }
    getDimensions() {
        return [this.x, this.y, this.width, this.height];
    }
    draw(ctx: any, drawPos: any) {
        this.timer++;
        if (this.timer > this.frameDelay) {
            this.counter+=1;
            this.timer = 0;
        }
        if (this.counter > 55-1) {
            this.objects.removeExplosion(this);
        }
        ctx.drawImage(
            this.image, this.width*this.counter, 0, this.width, this.height,
            drawPos[0], drawPos[1], this.width, this.height
        );
    }
}