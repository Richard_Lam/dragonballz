// @ts-ignore
import { actions } from "./Controls.tsx";
// @ts-ignore
import { Objects } from "./Objects.tsx";
// @ts-ignore
import { Character, Direction } from "./player/Character.tsx";
// @ts-ignore
import { getDistance, randomInt } from "./Utils.tsx";

enum AIState {
    fireKiBlastsFromDistance,
    attack,
    fireKiWavesFromDistance,
    fireMaxPowerKiWavesFromDistance,
    rapidFireKiBlasts,
    selfExplosion,
    transform
}

const getClosestEnemy = (self: Character, allCharacters: Character[]) => {
    let distance = 1_000_000;
    let target = null;
    for (let i=0; i < allCharacters.length; i++) {
        const currentCharacter = allCharacters[i];
        const currentDistance = getDistance(self.getCenter(), currentCharacter.getCenter());
        if (currentDistance < distance && self !== currentCharacter && self.team !== currentCharacter.team && currentCharacter.stats.health > 0) {
            target = currentCharacter;
            distance = currentDistance;
        }
    }
    return target;
}

export class AI {

    state: AIState
    stateTimer: number;
    targetTimer: number;
    timer: number;
    controls: actions;
    objects: Objects;
    kiBlastToFire: number;
    target: Character;
    npc: Character;

    chaseStates: {
        delay: number;
        wasAttacking: boolean;
    };
    fleeStates: {
        delay: number;
        wasMovingLeft: boolean;
    }
    runIfTargetIsChargingKiBlastStates: {
        state: number;
        moveUp: number;
        moveDown: number;
    }
    rapidFireStates: {
        state: number
    }
    explosionStates: {
        state: number
    }

    
    constructor(objects, npc: Character) {
        this.state = AIState.attack;
        this.stateTimer = 0;
        this.targetTimer = 0;
        this.timer = 0;
        this.controls = {up: 0, down: 0, left: 0, right: 0, c: 0, x: 0};
        this.objects = objects;
        this.kiBlastToFire = 0;
        this.npc = npc;

        this.chaseStates = {
            delay: 0,
            wasAttacking: false
        };
        this.fleeStates = {
            delay: 0,
            wasMovingLeft: false
        }
        this.rapidFireStates = {
            state: 0
        }
        this.runIfTargetIsChargingKiBlastStates = {
            state: 0,
            moveUp: 0,
            moveDown: 0
        }
        this.explosionStates = {
            state: 0
        }
        setTimeout(() => {
            this.target = getClosestEnemy(npc, this.objects.getCharacters());
        }, 1);
    }
    getActions(): actions {
        if (this.target === null) {
            return {up: 0, down: 0, left: 0, right: 0, c: 0, x: 0};
        }
        if (this.target !== null && this.target.stats.health <= 0) {
            // If current target dead, find new target
            this.target = getClosestEnemy(this.npc, this.objects.getCharacters());
            if (this.target === null) {
                return {up: 0, down: 0, left: 0, right: 0, c: 0, x: 0};
            }
        }

        // Change target
        this.handleTarget();

        // Change states
        this.handleState();

        // Execute current state's actions
        switch (this.state) {
            case AIState.attack:
                return this.attack();
            case AIState.fireKiBlastsFromDistance:
                return this.fireKiBlasts(0);
            case AIState.fireKiWavesFromDistance:
                return this.fireKiBlasts(100);
            case AIState.fireMaxPowerKiWavesFromDistance:
                return this.fireKiBlasts(199);
            case AIState.rapidFireKiBlasts:
                return this.rapidfireKiBlasts();
            case AIState.selfExplosion:
                return this.selfExplosion();
            case AIState.transform:
                return this.transform();
        }
    }
    handleTarget() {
        if (this.targetTimer <= 0) {
            this.target = getClosestEnemy(this.npc, this.objects.getCharacters());
            this.targetTimer = randomInt(1000, 2000);
        }
        this.targetTimer--;
    }
    handleState() {
        if (this.npc.specialMoves.selfExplosion && this.npc.stats.health < 100) {
            this.state = AIState.selfExplosion;
            return;
        }
        if (this.npc.canTransform()) {
            this.state = AIState.transform;
            return;
        }
        if (this.stateTimer <= 0) {
            switch(randomInt(0, 5)) {
                case 0:
                case 1:
                    this.state = AIState.attack;
                    this.stateTimer = randomInt(100, 250);
                    return;
                case 2:
                    this.state = AIState.fireKiBlastsFromDistance;
                    this.stateTimer = randomInt(100, 200);
                    return;
                case 3:
                    this.state = AIState.fireKiWavesFromDistance;
                    this.stateTimer = randomInt(150, 250);
                    return;
                case 4:
                    this.state = AIState.rapidFireKiBlasts;
                    this.stateTimer = randomInt(150, 250);
                    return;
                case 5:
                    this.state = AIState.fireMaxPowerKiWavesFromDistance;
                    this.stateTimer = randomInt(150, 250);
            }
        }
        this.stateTimer--;
    }
    attack() {
        const REACTION_TIME = 20;

        const target = this.target;
        const targetCenter = target.getCenter();
        const npcCenter = this.npc.getCenter();

        if (getDistance(npcCenter, targetCenter) > 20) {
            // If was attacking, add delay
            if (this.chaseStates.wasAttacking) {
                this.chaseStates.delay = randomInt(REACTION_TIME-2, REACTION_TIME+2);
                this.chaseStates.wasAttacking = false;
            }
            if (this.chaseStates.delay > 0) {
                this.chaseStates.delay--;
                return this.punching();
            }
            return this.chasePlayer(this.npc);
        }
        this.chaseStates.wasAttacking = true;
        return this.punching();
    }
    chasePlayer(npc: Character) {
        const targetCenter = this.target.getCenter();
        const npcCenter = npc.getCenter();

        const actions = {up: 0, down: 0, left: 0, right: 0, c: 1, x: 0, z: 0};
        if (Math.abs(targetCenter[0] - npcCenter[0]) > 10) {
            if (targetCenter[0] < npcCenter[0]) {
                actions.left = 1;
            }
            else {
                actions.right = 1;
            }
        }
        if (Math.abs(targetCenter[1] - npcCenter[1]) > 10) {
            if (targetCenter[1] < npcCenter[1]) {
                actions.up = 1;
            }
            else {
                actions.down = 1;
            }
        }
        return actions;
    }
    runIfTargetIsChargingKiBlast() {
        let actions: null | actions = null;
        if (this.target.isChargingKiWave()) {
            // target right of player
            if (this.target.x - this.npc.x > 0 && this.target.direction === Direction.right) {
                return null;
            }
            // target left of player
            if (this.target.x - this.npc.x < 0 && this.target.direction === Direction.left) {
                return null;
            }

            if (this.getDistanceFromTarget() < 100) {
                actions = this.getAwayFromPlayer();
            }
            else {
                if (this.runIfTargetIsChargingKiBlastStates.state === 0) {
                    if (randomInt(0, 1) === 0) {
                        this.runIfTargetIsChargingKiBlastStates.moveDown = randomInt(50, 100);
                    }
                    else {
                        this.runIfTargetIsChargingKiBlastStates.moveUp = randomInt(50, 100);
                    }
                    this.runIfTargetIsChargingKiBlastStates.state = 1;
                }
            }
        }

        if (this.runIfTargetIsChargingKiBlastStates.moveDown > 0) {
            actions = {up: 0, down: 1, left: 0, right: 0, c: 1, x: 0, z: 0}
            this.runIfTargetIsChargingKiBlastStates.moveDown--;
        }
        else if (this.runIfTargetIsChargingKiBlastStates.moveUp > 0) {
            actions = {up: 1, down: 0, left: 0, right: 0, c: 1, x: 0, z: 0}            
            this.runIfTargetIsChargingKiBlastStates.moveUp--;
        }
        else {
            this.runIfTargetIsChargingKiBlastStates.state = 0;
        }
        return actions;
    }
    getAwayFromPlayer() {
        const targetCenter = this.target.getCenter();
        const npcCenter = this.npc.getCenter();

        const actions = {up: 0, down: 0, left: 0, right: 0, c: 1, x: 0, z: 0};
        // X
        // Player left of npc
        if (targetCenter[0] < npcCenter[0]) {
            actions.right = 1;
        }
        else {
            actions.left = 1;
        }

        const backgroundDimensions = this.objects.getBackground().getDimensions();
        if (Math.abs(npcCenter[0] - backgroundDimensions[0]) < 30) {
            actions.left = 0;
            actions.right = 1;
        }
        if (Math.abs(npcCenter[0] - backgroundDimensions[0] + backgroundDimensions[2]) < 30) {
            actions.right = 0;
            actions.left = 1;
        }
        return actions;
    }
    punching() {
        // 5 punches per second
        const actions = {up: 0, down: 0, left: 0, right: 0, c: 0, x: 1, z: 0};
        this.timer += 1;
        if (this.timer < 20) {
            return actions;
        }
        actions.x = 0;
        this.timer = 0;
        return actions;
    }
    charge() {
        return {up: 0, down: 0, left: 0, right: 0, c: 1, x: 0, z: 0};
    }
    fireKiBlasts(minimumEnergy: number) {
        // Decide what to fire
        if (this.kiBlastToFire === 0) {
            this.kiBlastToFire = minimumEnergy + (200 - minimumEnergy)*Math.random();
        }

        const targetCenter = this.target.getCenter();
        const npcCenter = this.npc.getCenter();
        // If player is near, run
        if (getDistance(targetCenter, npcCenter) < 100) {
            return this.getAwayFromPlayer();
        }

        // Charge up enough energy
        if (this.npc.kiBlastDamage === 0 && this.npc.stats.energy < this.kiBlastToFire) {
            return this.charge();
        }
        const actions = {up: 0, down: 0, left: 0, right: 0, c: 0, x: 0, z: 0};

        // Start Firing
        actions.z = 1;
        const [targetX, targetY] = this.target.getDimensions();
        const [npcX, npcY] = this.npc.getDimensions();

        let needAlignPositions = false;
        // Align positions
        if (Math.abs(npcY - targetY) > 10) {
            if (npcY < targetY) {
                actions.down = 1;
            }
            else {
                actions.up = 1;
            }
            actions.c = 1;
            needAlignPositions = true;
        }
        // Face player
        if (targetX > npcX) {
            this.npc.setDirection(Direction.right);
        }
        else {
            this.npc.setDirection(Direction.left);
        }
        // Release Firing (and if player is aligned)
        if ((this.npc.kiBlastDamage >= this.kiBlastToFire || this.npc.stats.energy === 0) && !needAlignPositions) {
            actions.z = 0;
            this.kiBlastToFire = 0;
        }
        return actions;
    }
    rapidfireKiBlasts() {
        // If out of energy, charge
        if (this.npc.stats.energy < 10) {
            this.rapidFireStates.state = 0;
        }

        // Decide what to fire
        if (this.rapidFireStates.state === 0) {
            this.kiBlastToFire = 100 + 100*Math.random();
            this.rapidFireStates.state = 2;
        }

        const targetCenter = this.target.getCenter();
        const npcCenter = this.npc.getCenter();
        // If player is near, run
        if (getDistance(targetCenter, npcCenter) < 100) {
            return this.getAwayFromPlayer();
        }

        // Charge up enough energy
        if (this.npc.kiBlastDamage === 0 && this.npc.stats.energy < this.kiBlastToFire && this.rapidFireStates.state === 2) {
            return this.charge();
        }
        const actions = {up: 0, down: 0, left: 0, right: 0, c: 0, x: 0, z: 0};

        if (this.rapidFireStates.state === 2) {
            this.rapidFireStates.state = 1;
        }
        // Start Firing
        actions.z = 1;
        const [targetX, targetY] = this.target.getDimensions();
        const [npcX, npcY] = this.npc.getDimensions();

        let needAlignPositions = false;
        // Align positions
        if (Math.abs(npcY - targetY) > 10) {
            if (npcY < targetY) {
                actions.down = 1;
            }
            else {
                actions.up = 1;
            }
            needAlignPositions = true;
        }
        // Face player
        if (targetX > npcX) {
            this.npc.setDirection(Direction.right);
        }
        else {
            this.npc.setDirection(Direction.left);
        }
        // Release Firing (and if player is aligned)
        if (this.npc.kiBlastDamage >= 5 && !needAlignPositions) {
            actions.z = 0;
        }
        return actions;
    }
    selfExplosion() {
        if (this.npc.stats.energy < 200) {
            this.explosionStates.state = 0;
        }
        else if (getDistance(this.npc.getCenter(), this.target.getCenter()) > 20) {
            this.explosionStates.state = 1;
        }
        else {
            this.explosionStates.state = 2;
        }
        switch (this.explosionStates.state) {
            case 0:
                return this.charge();
            case 1:
                return this.chasePlayer(this.npc);
            case 2:
                return {up: 0, down: 0, left: 0, right: 0, c: 1, x: 1, z: 0};
        }
    }
    transform() {
        if (this.npc.energy < 200) {
            return this.charge();
        }
        return {up: 0, down: 0, left: 0, right: 0, c: 1, x: 1, z: 0};
    }
    getDistanceFromTarget() {
        const targetCenter = this.target.getCenter();
        const npcCenter = this.npc.getCenter();
        return getDistance(targetCenter, npcCenter);
    }
}