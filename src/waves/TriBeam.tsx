// @ts-ignore
import { KiWave } from "../KiWave.tsx";
// @ts-ignore
import { Objects } from "../Objects.tsx";
// @ts-ignore
import { Character, Direction } from "../player/Character.tsx";
// @ts-ignore
import { Explosions } from "../Explosions.tsx";
// @ts-ignore
import { isRectsIntersect, Polygon, calculateKiWaveDamage } from "../Utils.tsx";

export class TriBeam extends KiWave {

    collidedCharacters: {pushBackTimer: number, character: Character}[];

    // Note: each tick in the game is 60fps
    readonly pushBackAmount = 3*60;

    constructor(objects: Objects, x, y, direction, owner, damage: number, ownerSpecialAttack: number) {
        super(objects, x, y, direction, owner, damage, ownerSpecialAttack);
        this.speed = 8;
        const head = {
            x: x, y: y, width: 70, height: 60, baseWidth: 0,
            sprite: document.getElementById("super_yellow_ki_wave_head")
        };
        const body = {
            x: x, y: y, width: 10, height: 60, baseWidth: 10,
            sprite: document.getElementById("super_yellow_ki_wave_body")
        };
        const base = {
            x: x, y: y, width: 40, height: 60, baseWidth: 0,
            sprite: document.getElementById("super_yellow_ki_wave_base")
        };
        [this.kiWaveHead, this.kiWaveBody, this.kiWaveBase] = [head, body, base];
        this.collidedCharacters = [];
    }
    // @Override
    update() {
        // Move Head
        if (this.direction === Direction.left) {
            this.kiWaveHead.x -= this.speed;

            this.kiWaveBody.x -= this.speed;
        }
        else {
            this.kiWaveHead.x += this.speed;
            if (!this.grow) {
                this.kiWaveBody.x += this.speed;
            }
        }
        if (this.grow) {
            this.kiWaveBody.width += this.speed;
        }
        for (const character of this.objects.getCharacters()) {
            const charPolygon = new Polygon(character.getDimensions());
            const kiBlastPolygon = new Polygon(this.getDimensions());
            if (this.owner !== character && character.getTeam() !== this.owner.getTeam() && isRectsIntersect(charPolygon, kiBlastPolygon)) {
                this.handleOnCollide(character);
            }
        }
        for (const kiBlast of this.objects.getKiBlasts()) {
            const kiBlastPolygon = new Polygon(kiBlast.getDimensions());
            const thisKiBlastPolygon = new Polygon(this.getDimensions());
            if (this.owner !== kiBlast.owner && this.owner.getTeam() !== kiBlast.owner.getTeam() && isRectsIntersect(kiBlastPolygon, thisKiBlastPolygon)) {
                this.handleCollideKiBlast(kiBlast);
            }
        }
        // Keep character collided on tri beam
        if (0 < this.kiWaveHead.x && this.kiWaveHead.x < this.objects.getBackground().getDimensions()[2] - this.kiWaveHead.width) {
            for (const obj of this.collidedCharacters) {
                if (obj.pushBackTimer < this.pushBackAmount) {
                    console.log(obj.pushBackTimer);
                    const character = obj.character;
                    if (this.direction === Direction.left) {
                        character.x = this.kiWaveHead.x - (character.width / 2);
                    }
                    // Right
                    else {
                        character.x = this.kiWaveHead.x + this.kiWaveHead.width - (character.width / 2);
                    }
                    character.y = this.kiWaveHead.y + this.kiWaveHead.height / 2 - (character.height / 2);
                    obj.pushBackTimer++;
                }
            }
        }

        if (!this.owner.isFiringKiWave()) {
            this.grow = false;
        }
        this.timer++;
    }
    // @Override
    handleOnCollide(character) {
        if (this.collidedCharactersIndexOf(character) === -1) {
            this.objects.addExplosion(new Explosions(this.kiWaveHead.x, this.kiWaveHead.y, this.objects, this.damage));
            if (character.specialMoves.absorbEnergy) {
                character.addEnergy(this.damage);
            }
            else {
                character.removeHealth(calculateKiWaveDamage(this.damage, this.ownerSpecialAttack, character.stats.defense));
            }
            this.collidedCharacters.push({pushBackTimer: 0, character: character});
        }
    }
    collidedCharactersIndexOf(character) {
        for (let i=0; i < this.collidedCharacters.length; i++) {
            if (this.collidedCharacters[i].character === character) {
                return i;
            }
        }
        return -1;
    }
}