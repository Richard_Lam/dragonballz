export class Background {

    x: number;
    y: number;
    width: number;
    height: number;
    image: HTMLElement | null;

    constructor(x: number, y: number, img: string) {
        this.x = x;
        this.y = y;
        this.image = document.getElementById(img);
        // @ts-ignore
        this.width = this.image.width;
        // @ts-ignore
        this.height = this.image.height;
    }
    getDimensions() {
        return [this.x, this.y, this.width, this.height];
    }
    getBounds() {
        return [this.x, this.y, this.x + this.width, this.y + this.height];
    }
    draw(ctx: any, drawPos: any) {
        if (this.image == null) {
            return;
        }
        ctx.drawImage(
            this.image, 0, 0, this.width, this.height,
            drawPos[0], drawPos[1], this.width, this.height
        );
    }
}
