// @ts-ignore
import { randomInt } from "./Utils.tsx";

export class Sounds {

    playAttackSound() {
        this.#playSound(`attack_hard_sound_${randomInt(1, 3)}.wav`);
    }
    playFireKiBlast() {
        this.#playSound("fire_ki_blast.wav");
    }
    playFireKiWave() {
        this.#playSound("fire_wave.wav");
    }
    playSpecialBeamCannon() {
        this.#playSound("special_beam_cannon.wav")
    }
    playTransform() {
        this.#playSound("transformation_sound.wav");
    }
    playDeathBeam() {
        this.#playSound("death_beam.wav");
    }
    playTriBeam() {
        this.#playSound("tri_beam.wav");
    }
    #playSound(str) {
        const audio = new Audio(`sounds/${str}`);
        audio.play();
    }
}