// @ts-ignore
import { KiBlast } from "./KiBlast.tsx";
// @ts-ignore
import { Objects } from "./Objects.tsx";

export class BigBangAttack extends KiBlast {
    
    frameDelay: number;
    frameTimer: number;
    frameCounter: number;

    constructor(objects: Objects, x: number, y: number, direction, owner, damage, ownerSpecialAttack) {
        super(objects, x, y, direction, owner, damage, ownerSpecialAttack);
        // @ts-ignore
        this.width = 80; // @ts-ignore
        this.height = 80; // @ts-ignore
        this.image = document.getElementById("big_bang_attack");
        this.frameDelay = 2;
        this.frameTimer = 0;
        this.frameCounter = 0; //@ts-ignore
        this.speed = 8;
    }
    draw(ctx, drawPos) {
        this.frameTimer++;
        if (this.frameTimer > this.frameDelay) {
            this.frameTimer = 0;
            this.frameCounter++;
        }
        if (this.frameCounter > 8-1) {
            this.frameCounter = 0;
        }
        ctx.drawImage( // @ts-ignore
            this.image, this.frameCounter*this.width, 0, this.width, this.height, // @ts-ignore
            drawPos[0], drawPos[1], this.width, this.height
        );
    }
}