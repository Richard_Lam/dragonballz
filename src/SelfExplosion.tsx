// @ts-ignore
import { Explosions } from "./Explosions.tsx";
// @ts-ignore
import { isRectsIntersect, Polygon, calculateSelfExplosionDamage } from "./Utils.tsx";

export class SelfExplosion {
    constructor(x, y, owner, objects, damage) {
        objects.addExplosion(new Explosions(x, y, objects, damage));
        const [width, height] = [100, 100];
        const dimensions = [x - (width/2), y - (height/2),width, height];
        for (const character of objects.getCharacters()) {
            const charPolygon = new Polygon(character.getDimensions());
            const kiBlastPolygon = new Polygon(dimensions);
            if (owner !== character && character.getTeam() !== owner.getTeam() && isRectsIntersect(charPolygon, kiBlastPolygon)) {
                character.removeHealth(calculateSelfExplosionDamage(damage, owner.stats.specialAttack, character.stats.defense));
            }
        }
    }
}