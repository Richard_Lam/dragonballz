// @ts-ignore
import { KiWave } from "./KiWave.tsx";
// @ts-ignore
import { Objects } from "./Objects.tsx";

export class DeathBeam extends KiWave {
    constructor(objects: Objects, x, y, direction, owner, damage: number, ownerSpecialAttack: number) {
        super(objects, x, y, direction, owner, damage*0.5, ownerSpecialAttack);
        this.speed = 30;
        const head = {
            x: x, y: y, width: 15, height: 10, baseWidth: 0,
            sprite: document.getElementById("thin_violet_ki_wave_head")
        };
        const body = {
            x: x, y: y, width: 10, height: 10, baseWidth: 10,
            sprite: document.getElementById("thin_violet_ki_wave_body")
        };
        const base = {
            x: x, y: y, width: 10, height: 10, baseWidth: 0,
            sprite: document.getElementById("thin_violet_ki_wave_base")
        };
        [this.kiWaveHead, this.kiWaveBody, this.kiWaveBase] = [head, body, base];
    }
}