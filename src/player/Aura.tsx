export class Aura {

    width: number;
    height: number;
    aura: HTMLElement | null;
    counter: number;

    constructor(color: string) {
        this.width = 101;
        this.height = 126;
        this.aura = color ? document.getElementById(`${color}_charging_aura`) : document.getElementById("blue_charging_aura");
        this.counter = 1;
    }
    draw(ctx, charDimensions) {
        const [x, y, width, height] = charDimensions;
        const [auraX, auraY] = [x + width/2 - this.width/2, y + height - this.height];
        ctx.drawImage(
            this.aura, this.width*Math.floor(this.counter / 10), 0, this.width, this.height,
            auraX, auraY, this.width, this.height
        );
        this.counter++;
        if (this.counter > 10*3-1) {
            this.counter = 0;
        }
    }
}