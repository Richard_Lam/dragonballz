// @ts-ignore
import { Character, Team } from "./Character.tsx";
// @ts-ignore
import { SoundsHandler } from "../SoundsHandler.tsx";
// @ts-ignore
import { AI } from "../AI.tsx";
// @ts-ignore
import { Objects } from "../Objects.tsx";

export class NPC {

    npc: Character;
    ai: AI;
    objects: Objects

    constructor(x, y, team: Team, character, objects, health, maxTransformations=1000) {
        this.npc = new Character(x, y, 54, 54, (new SoundsHandler()).getInstance(), team, character, health, maxTransformations);
        this.ai = new AI(objects, this.npc);
        this.objects = objects;
        this.objects.addNPC(this.npc);
    }
    update() {
        this.npc.update(this.ai.getActions(), this.objects);
    }
}