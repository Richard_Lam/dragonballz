// @ts-ignore
import { Aura } from "./Aura.tsx";
// @ts-ignore
import { actions } from "./Controls.tsx";
// @ts-ignore
import { EnvironmentUpdates } from "./Enviroment.tsx";
// @ts-ignore
import { StatsDisplay } from "./StatsDisplay.tsx";
// @ts-ignore
import { Objects } from "../Objects.tsx";
// @ts-ignore
import { intersect } from "../Utils.tsx";
// @ts-ignore
import { Sounds } from "../Sounds.tsx";
// @ts-ignore
import { KiBlast } from "../KiBlast.tsx";
// @ts-ignore
import { KiWave } from "../KiWave.tsx";
// @ts-ignore
import { calculatePunchDamage, calcBaseSpeed, getDistance } from "../Utils.tsx";
// @ts-ignore
import { DeathBeam } from "../DeathBeam.tsx";
// @ts-ignore
import { BigBangAttack } from "../BigBangAttack.tsx";
import statsJson from "./stats.json";
// @ts-ignore
import { SpecialBeamCannon } from "../SpecialBeamCannon.tsx";
// @ts-ignore
import { SelfExplosion } from "../SelfExplosion.tsx";
// @ts-ignore
import { Nexus } from "../components/ProtectTheNexusMenu/Nexus.tsx";
// @ts-ignore
import { TriBeam } from "../waves/TriBeam.tsx";
// @ts-ignore
import { Timer } from "../Timer.tsx";

export enum Direction {
    left,
    right
}

export enum State {
    standing,
    moving,
    attack1,
    attack2,
    charging,
    dead,
    charging_ki_blast,
    firing_ki_blast
}

enum AttackState {
    attacking1,
    attacking2
}

export enum Team {
    One,
    Two,
    Three
}

interface CharacterStats {
    health: number;
    attack: number;
    defense: number;
    specialAttack: number;
    speed: number;
    energy: number;
}

interface SpecialMoves {
    deathBeam: boolean;
    bigBangAttack: boolean;
    regeneration: boolean;
    specialBeamCannon: boolean;
    selfExplosion: boolean;
    absorbEnergy: boolean;
    infiniteEnergy: boolean;
    triBeam: boolean;
    healer: boolean;
    cantFireEnergy: boolean;
}
const emptySpecialMoves: SpecialMoves = {
    deathBeam: false,
    bigBangAttack: false,
    regeneration: false,
    specialBeamCannon: false,
    selfExplosion: false,
    absorbEnergy: false,
    infiniteEnergy: false,
    triBeam: false,
    healer: false,
    cantFireEnergy: false,
}

const stats = statsJson;

export class Character {
    
    name: string
    x: number;
    y: number;
    width: number;
    height: number;
    protected image: HTMLElement | null;
    direction: Direction;
    baseSpeed: number;
    state: State;
    attackState: AttackState;
    maxHealth: number;
    currentNumOfTransformations: number;
    maxTransformations: number;

    canAttack: boolean;
    isChargingKiBlast: boolean;
    isSuperChargingKiBlast: boolean;
    
    kiBlastDamage: number;

    protected stats: CharacterStats;
    statsDisplay: StatsDisplay;
    aura: Aura;
    sounds: Sounds;
    team: Team | number;
    protected specialMoves: SpecialMoves;

    maxEnergyLimit = 200;
    healerTimer = new Timer(20);

    constructor(x: number, y: number, width: number, height: number, sounds: Sounds, team: Team, id: string, health: number, maxTransformations = 1000) {
        const characterStats = stats[id];
        this.name = id;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.image = document.getElementById(characterStats["image"]);
        this.direction = Direction.left;
        this.state = State.standing;
        this.attackState = AttackState.attacking1;
        this.canAttack = true;
        this.isChargingKiBlast = false;
        this.isSuperChargingKiBlast = false;

        this.kiBlastDamage = 0;
        this.stats = {health: health,
            attack: characterStats["attack"],
            defense: characterStats["defense"],
            specialAttack: characterStats["specialAttack"],
            speed: characterStats["speed"],
            energy: 0
        };
        this.maxHealth = 500;
        this.currentNumOfTransformations = 0;
        this.maxTransformations = maxTransformations;

        this.baseSpeed = calcBaseSpeed(this.stats.speed);
        this.statsDisplay = new StatsDisplay();
        this.sounds = sounds;
        this.team = team;

        this.specialMoves = {...emptySpecialMoves};
        const specialMoves = characterStats["specialMoves"];
        for (const specialMove in specialMoves) {
            this.specialMoves[specialMove] = specialMoves[specialMove];
        }

        if (characterStats["width"]) {
            this.width = characterStats["width"];
        }
        if (characterStats["height"]) {
            this.height = characterStats["height"];
        }
        this.aura = new Aura(characterStats["aura"]);
    }
    setCharacter(id: string) {
        this.name = id;
        const characterStats = stats[id];
        this.image = document.getElementById(characterStats["image"]);
        this.setStats(characterStats);
    }
    update(actions: actions, objects: Objects) {
        if (this.state === State.dead) {
            return;
        }

        // Regeneration
        if (this.specialMoves.regeneration && this.stats.health < this.maxHealth) {
            this.stats.health+=0.1;
            if (this.stats.health > this.maxHealth) {
                this.stats.health = this.maxHealth;
            }
        }

        let isAttacking = false;
        if (actions.x) {
            if (this.canAttack) {
                this.canAttack = false;
                this.state = this.attackState === AttackState.attacking1 ? State.attack1 : State.attack2;
                this.attackState = this.attackState === AttackState.attacking1 ? AttackState.attacking2 : AttackState.attacking1;
                this.#attack(objects);
            }
            isAttacking = true;
        }
        else {
            this.canAttack = true;
        }

        if (this.stats.energy === 200 && actions.x && actions.c) {
            // TRANSFORM
            if (this.canTransform()) {
                this.transform();
            }
            // EXPLODE
            else if (this.specialMoves.selfExplosion) {
                console.log(this.specialMoves.selfExplosion);
                const center = this.getCenter();
                new SelfExplosion(center[0], center[1], this, objects, this.stats.energy);
                this.removeHealth(1000);
                this.stats.energy = 0;
                return;
            }
        }

        // Charge ki blast
        let isChargingKiBlast = false;
        let isFiringKiBlast = false;
        if (actions.z) {
            this.state = State.charging_ki_blast;
            if (this.stats.energy > 0) {
                if (!this.specialMoves.infiniteEnergy) {
                    this.stats.energy-=1;
                }
                if (this.kiBlastDamage < 200) {
                    this.kiBlastDamage+=1;
                }
            }
            if (this.kiBlastDamage > 170) {
                this.isSuperChargingKiBlast = true;
            }
            isChargingKiBlast = true;
            this.isChargingKiBlast = true;
        }
        // Release ki blast (was charging ki blast and not pressing z anymore)
        if (this.isChargingKiBlast && !actions.z) {
            this.#handleReleaseKiBlast(objects);
            isFiringKiBlast = true;
            this.isChargingKiBlast = false;
            this.isSuperChargingKiBlast = false;
        }

        let isMoving = this.#handleMovement(actions, objects);

        let isCharging = false;
        if (actions.c) {
            if (!isMoving && !isChargingKiBlast && !isFiringKiBlast) {
                this.state = State.charging;
                this.addEnergy(1);
                isCharging = true;
                // specialMoves: healer
                if (this.specialMoves.healer) {
                    for (const character of objects.getCharacters()) {
                        if (character !== this && character.getTeam() === this.getTeam() && getDistance(character.getCenter(), this.getCenter()) < 60) {
                            this.healerTimer.update(() => character.addHealth(10));
                        }
                    }
                }
            }
        }
        // If firing ki blast, keep firing ki blast
        if (this.state === State.firing_ki_blast) {
            return;
        }
        if (!isMoving && !isChargingKiBlast && !isCharging && !isAttacking) {
            this.state = State.standing;
        }
        return;
    }
    addEnergy(num) {
        if (this.stats.energy < this.maxEnergyLimit) {
            this.stats.energy+=num;
        }
        if (this.stats.energy > this.maxEnergyLimit) {
            this.stats.energy = this.maxEnergyLimit;
        }
    }
    setStats(stats) {
        this.stats.attack = stats.attack;
        this.stats.defense = stats.defense;
        this.stats.specialAttack = stats.specialAttack;
        this.stats.speed = stats.speed;
        this.baseSpeed = calcBaseSpeed(this.stats.speed);

        if (stats["width"]) {
            this.width = stats["width"];
        }
        if (stats["height"]) {
            this.height = stats["height"];
        }
        this.aura = new Aura(stats["aura"]);

        this.specialMoves = {...emptySpecialMoves};
        const specialMoves = stats["specialMoves"];
        for (const specialMove in specialMoves) {
            this.specialMoves[specialMove] = specialMoves[specialMove];
        }
    }
    canTransform() {
        return stats[this.name]["transform"] !== undefined && this.currentNumOfTransformations < this.maxTransformations;
    }
    transform() {
        this.setCharacter(stats[this.name]["transform"]);
        this.stats.energy = 0;
        this.sounds.playTransform();
        this.currentNumOfTransformations++;
    }
    updateFromEnvironment(updates: EnvironmentUpdates) {
        if (updates.dealDamage) {
            this.removeHealth(updates.dealDamage);
        }
    }
    setDirection(direction: Direction) {
        this.direction = direction;
    }
    getTeam() {
        return this.team;
    }
    setTeam(team: Team) {
        this.team = team;
    }
    #attack(objects: Objects) {
        const characters: Character[] = objects.getCharacters();
        for (const character of characters) {
            if (character !== this && character.getTeam() !== this.getTeam() && intersect(character.getDimensions(), this.getDimensions())) {
                const damage = calculatePunchDamage(this.stats.attack, character.stats.defense);
                character.removeHealth(damage);
                this.sounds.playAttackSound();
            }
        }
        const nexuses: Nexus[] = objects.getNexuses();
        for (const nexus of nexuses) {
            if (nexus !== this && nexus.getTeam() !== this.getTeam() && intersect(nexus.getDimensions(), this.getDimensions())) {
                nexus.takeDamage(this);
            }
        }
    }
    #handleMovement(actions: actions, objects: Objects) {
        let isMoving = false;
        let finalSpeed = this.baseSpeed;
        const superSpeedMultiplier = 2;
        const background = objects.getBackground();
        const [backgroundX, backgroundY, backgroundWidth, backgroundHeight] = background.getDimensions();
        if (actions.up) {
            this.state = State.standing;
            if (actions.c) {
                finalSpeed = this.baseSpeed * superSpeedMultiplier;
            }
            this.y-=finalSpeed;
            if (this.y <= backgroundY) {
                this.y = backgroundY;
            }
            isMoving = true;
        }
        if (actions.down) {
            this.state = State.standing;
            if (actions.c) {
                finalSpeed = this.baseSpeed * superSpeedMultiplier
            }
            this.y+=finalSpeed;
            if (this.y + this.height >= backgroundY + backgroundHeight) {
                this.y = backgroundY + backgroundHeight - this.height;
            }
            isMoving = true;
        }
        if (actions.left) {
            this.state = State.moving;
            if (actions.c) {
                finalSpeed = this.baseSpeed * superSpeedMultiplier
            }
            this.x-=finalSpeed;
            if (this.x <= backgroundX) {
                this.x = backgroundX;
            }
            this.direction = Direction.left;
            isMoving = true;
        }
        if (actions.right) {
            this.state = State.moving;
            if (actions.c) {
                finalSpeed = this.baseSpeed * superSpeedMultiplier
            }
            this.x+=finalSpeed;
            if (this.x + this.width >= backgroundX + backgroundWidth) {
                this.x = backgroundX + backgroundWidth - this.width;
            }
            this.direction = Direction.right;
            isMoving = true;
        }
        return isMoving;
    }
    #handleReleaseKiBlast(objects: Objects) {
        this.state = State.firing_ki_blast;
        if (this.kiBlastDamage > 100) {
            // Handle ki wave
            const [kiBlastX, kiBlastY] = this.#getKiBlastSpawnLocation();

            // Big Bang Attack
            if (this.kiBlastDamage > 198 && this.specialMoves.bigBangAttack) {
                new BigBangAttack(objects, kiBlastX, kiBlastY, this.direction, this, this.kiBlastDamage, this.stats.specialAttack);
            }
            // Special Beam Cannon
            else if (this.kiBlastDamage > 198 && this.specialMoves.specialBeamCannon) {
                new SpecialBeamCannon(objects, kiBlastX, kiBlastY, this.direction, this, this.kiBlastDamage, this.stats.specialAttack);
                this.sounds.playSpecialBeamCannon();
            }
            else if (this.kiBlastDamage > 198 && this.specialMoves.triBeam) {
                new TriBeam(objects, kiBlastX, kiBlastY, this.direction, this, this.kiBlastDamage, this.stats.specialAttack);
                this.sounds.playTriBeam();
            }
            else {
                new KiWave(objects, kiBlastX, kiBlastY, this.direction, this, this.kiBlastDamage, this.stats.specialAttack);
                this.sounds.playFireKiWave();
            }
            this.kiBlastDamage = 0;
        }
        else if (this.kiBlastDamage > 0) {
            // Handle ki blast
            const [kiBlastX, kiBlastY] = this.#getKiBlastSpawnLocation();

            // Death beam
            if (this.kiBlastDamage < 30 && this.specialMoves.deathBeam) {
                new DeathBeam(objects, kiBlastX, kiBlastY, this.direction, this, this.kiBlastDamage, this.stats.specialAttack);
                this.sounds.playDeathBeam();
            }
            else {
                new KiBlast(objects, kiBlastX, kiBlastY, this.direction, this, this.kiBlastDamage, this.stats.specialAttack);
                this.sounds.playFireKiBlast();
            }
            this.kiBlastDamage = 0;
        }
    }
    isFiringKiWave() {
        return this.state === State.firing_ki_blast;
    }
    isChargingKiWave() {
        return this.state === State.charging_ki_blast;
    }
    removeHealth(damage: number) {
        this.stats.health -= damage;
        if (this.stats.health <= 0) {
            this.state = State.dead;
        }
        if (this.stats.health <= -100) {
            // Remove corpse from screen
            this.x = -1000;
            this.y = -1000;
        }
    }
    addHealth(health: number) {
        if (this.stats.health > 0) {
            this.stats.health += health;
            if (this.stats.health > this.maxHealth) {
                this.stats.health = this.maxHealth;
            }
        }
    }
    getDimensions() {
        return [this.x, this.y, this.width, this.height];
    }
    getCenter() {
        return [this.x + this.width/2, this.y + this.height/2];
    }
    #getKiBlastSpawnLocation() {
        let xLocation = this.x;
        if (this.direction === Direction.right) {
            xLocation = this.x + (this.width * 3 / 4);
        }
        return [xLocation, this.y + this.height / 2];
    }
    draw(ctx: any, drawPos: any) {
        if (this.image == null) {
            return;
        }
        let imageYoffset = 0;
        if (this.state === State.standing) {
            imageYoffset = 0;
        }
        else if (this.state === State.moving) {
            imageYoffset = 1;
        }
        else if (this.state === State.attack1) {
            imageYoffset = 2;
        }
        else if (this.state === State.attack2) {
            imageYoffset = 3;
        }
        else if (this.state === State.charging) {
            imageYoffset = 4;
        }
        else if (this.state === State.dead) {
            imageYoffset = 5;
        }
        else if (this.state === State.charging_ki_blast || this.state === State.firing_ki_blast) {
            imageYoffset = 6;
            if (this.isSuperChargingKiBlast) {
                imageYoffset = 7;
            }
        }

        let imageXoffset = 0;
        if (this.direction === Direction.left) {
            imageXoffset = 1;
        }

        ctx.drawImage(
            this.image, this.width*imageXoffset, this.height*imageYoffset, this.width, this.height,
            drawPos[0], drawPos[1], this.width, this.height
        );
        if (this.state === State.charging) {
            this.aura.draw(ctx, [drawPos[0], drawPos[1], this.width, this.height]);
        }
        this.statsDisplay.draw(ctx, [drawPos[0], drawPos[1], this.width], this.stats.health, this.stats.energy);
    }
}
