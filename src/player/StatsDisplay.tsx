// @ts-ignore
import { minZero } from "../Utils.tsx";

export class StatsDisplay {
    
    width: number;
    healthBarHeight: number;
    energyBarHeight: number;
    healthBarColors: (HTMLElement | null)[];
    energyBarColors: (HTMLElement | null)[];

    constructor() {
        this.width = 200;
        this.healthBarHeight = 20;
        this.energyBarHeight = 10;
        this.healthBarColors = [
            document.getElementById("gray_health_bar"),
            document.getElementById("red_health_bar"), // 0-100
            document.getElementById("orange_health_bar"), // 101-200
            document.getElementById("yellow_health_bar"), // 201-300
            document.getElementById("green_health_bar"), // 301-400
            document.getElementById("blue_health_bar") // 401-500
        ];
        this.energyBarColors = [
            document.getElementById("gray_health_bar"),
            document.getElementById("orange_charging_bar"),
            document.getElementById("blue_charging_bar")
        ]
    }
    draw(ctx, characterDimensions, health, energy) {
        const [charX, charY, charWidth] = characterDimensions;
        const [x, y] = [charX + charWidth/2 - this.width/2, charY - 80];
        const topColorIndex = Math.floor(minZero(health+99.999) / 100);
        const bottomColorIndex = minZero(topColorIndex - 1);
        let charHealth = minZero(health % 100);
        if (health === 0) {
            charHealth = 0;
        }
        else if (charHealth === 0) {
            charHealth = 100;
        }
        ctx.drawImage(
            this.healthBarColors[bottomColorIndex], 0, 0, 1, this.healthBarHeight,
            x, y, this.width, this.healthBarHeight
        );
        ctx.drawImage(
            this.healthBarColors[topColorIndex], 0, 0, 1, this.healthBarHeight,
            x, y, this.width * charHealth / 100, this.healthBarHeight
        );

        this.drawChargingBar(ctx, characterDimensions, energy);
    }
    drawChargingBar(ctx, characterDimensions, energy) {
        const [charX, charY, charWidth] = characterDimensions;
        const [x, y] = [charX + charWidth/2 - this.width/2, charY - 50];
        const topColorIndex = Math.floor(minZero(energy+99.999) / 100);
        const bottomColorIndex = minZero(topColorIndex - 1);
        let charEnergy = minZero(energy % 100);
        if (energy === 0) {
            charEnergy = 0;
        }
        else if (charEnergy === 0) {
            charEnergy = 100;
        }
        ctx.drawImage(
            this.energyBarColors[bottomColorIndex], 0, 0, 1, this.energyBarHeight,
            x, y, this.width, this.energyBarHeight
        );
        ctx.drawImage(
            this.energyBarColors[topColorIndex], 0, 0, 1, this.energyBarHeight,
            x, y, this.width * charEnergy / 100, this.energyBarHeight
        );
    }
}
