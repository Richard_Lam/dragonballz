// @ts-ignore
import { KiWave } from "./KiWave.tsx";
// @ts-ignore
import { Objects } from "./Objects.tsx";
// @ts-ignore
import { Direction, Character } from "./player/Character.tsx";
// @ts-ignore
import { minZero } from "./Utils.tsx";
// @ts-ignore
import { Explosions } from "./Explosions.tsx";
// @ts-ignore
import { calculateKiWaveDamage } from "./Utils.tsx";

export class SpecialBeamCannon extends KiWave {

    collidedCharacters: Character[];

    constructor(objects: Objects, x, y, direction, owner, damage: number, ownerSpecialAttack: number) {
        super(objects, x, y, direction, owner, damage, ownerSpecialAttack);
        this.speed = 6;
        x = direction === Direction.left ? x - 12 : x;
        const head = {
            x: x, y: y, width: 14, height: 20, baseWidth: 0,
            sprite: document.getElementById("special_beam_cannon_head")
        };
        const body = {
            x: x, y: y, width: 12, height: 20, baseWidth: 12,
            sprite: document.getElementById("special_beam_cannon_body")
        };
        const base = {
            x: x, y: y, width: 12, height: 20, baseWidth: 0,
            sprite: document.getElementById("special_beam_cannon_base")
        };
        [this.kiWaveHead, this.kiWaveBody, this.kiWaveBase] = [head, body, base];
        this.collidedCharacters = [];
    }
    // Overrides handleOnCollide in KiWave class
    handleOnCollide(character) {
        if (this.collidedCharacters.indexOf(character) === -1) {
            this.objects.addExplosion(new Explosions(this.kiWaveHead.x, this.kiWaveHead.y, this.objects, this.damage));
            if (character.specialMoves.absorbEnergy) {
                character.addEnergy(this.damage);
            }
            else {
                character.removeHealth(calculateKiWaveDamage(this.damage, this.ownerSpecialAttack, character.stats.defense));
            }
            this.collidedCharacters.push(character);
        }
    }
    draw(ctx, drawPos: any) {

        let imageXoffset = 0;
        let imageYoffset = 0;
        if (this.direction === Direction.left) {
            imageXoffset = 1;
        }
        if (Math.floor(this.timer / 5) % 2 === 0) {
            imageYoffset = 1;
        }

        const [x, y] = [drawPos[2].cameraX, drawPos[2].cameraY];
        // Draw ki wave body
        if (this.direction === Direction.left) {
            const kiWaveBodyWidth = minZero(this.kiWaveBody.width - (this.kiWaveHead.width + this.kiWaveBase.width));
            for (let i=0; i < kiWaveBodyWidth / this.kiWaveBody.baseWidth; i++) {
                ctx.drawImage(
                    this.kiWaveBody.sprite, 0, 0, this.kiWaveBody.baseWidth, this.kiWaveBody.height,
                    this.kiWaveBody.x - x + this.kiWaveHead.width + i*this.kiWaveBody.baseWidth, this.kiWaveBody.y - y, this.kiWaveBody.baseWidth, this.kiWaveBody.height
                );
            }
        }
        else {
            const kiWaveBodyWidth = minZero(this.kiWaveBody.width - (this.kiWaveHead.width + this.kiWaveBase.width));
            for (let i=0; i < kiWaveBodyWidth / this.kiWaveBody.baseWidth; i++) {
                ctx.drawImage(
                    this.kiWaveBody.sprite, 0, 0, this.kiWaveBody.baseWidth, this.kiWaveBody.height,
                    this.kiWaveBody.x - x + this.kiWaveBase.width + i*this.kiWaveBody.baseWidth, this.kiWaveBody.y - y, this.kiWaveBody.baseWidth, this.kiWaveBody.height
                );
            }
        }
        // Draw ki wave base
        if (this.grow) {
            ctx.drawImage(
                this.kiWaveBase.sprite, this.kiWaveBase.width*imageXoffset, this.kiWaveBase.height*imageYoffset, this.kiWaveBase.width, this.kiWaveBase.height,
                this.kiWaveBase.x - x, this.kiWaveBase.y - y, this.kiWaveBase.width, this.kiWaveBase.height
            );
        }
        // Draw ki wave head
        ctx.drawImage(
            this.kiWaveHead.sprite, this.kiWaveHead.width*imageXoffset, 0, this.kiWaveHead.width, this.kiWaveHead.height,
            this.kiWaveHead.x - x, this.kiWaveHead.y - y, this.kiWaveHead.width, this.kiWaveHead.height
        );
    }
}