import './App.scss';
import { Background } from './Background.tsx';
import { Camera } from './Camera.tsx';
import { Controls } from './Controls.tsx';
import { NPC } from './player/NPC.tsx';
import { SoundsHandler } from './SoundsHandler.tsx';
import LoadImages from './components/LoadImages.tsx';
import { Objects } from "./Objects.tsx";
import { Character } from "./player/Character.tsx";
import { Team } from "./player/Character.tsx";
import { MainMenu } from "./components/MainMenu.tsx";
import { StoryModeMenu } from "./components/StoryModeMenu/StoryModeMenu.tsx";
import React, { useEffect } from 'react';
import { NextButton } from './components/NextButton.tsx';
import { ProtectTheNexusMenu } from './components/ProtectTheNexusMenu/ProtectTheNexusMenu.tsx';

function App() {

  const canvasWidth = 1500;
  const canvasHeight = 600;
  const [currentMenu, setCurrentMenu] = React.useState("mainMenu");
  const [showNextButton, setShowNextButton] = React.useState(false);

  useEffect(() => {
    document.fonts.load('10pt "Felina Gothic"');
  }, []);

  return (
    <div className="App">
      <MainMenu visible={currentMenu === "mainMenu"} setCurrentMenu={setCurrentMenu}/>
      {showNextButton ? <NextButton runAnimation={runAnimation} setCurrentMenu={setCurrentMenu} setShowNextButton={setShowNextButton} /> : null}
      <StoryModeMenu
        startBattle={startBattle}
        visible={currentMenu === "storyModeMenu"}
        setCurrentMenu={setCurrentMenu}
        runAnimation={runAnimation}
        setShowNextButton={setShowNextButton}
      />
      <ProtectTheNexusMenu 
        visible={currentMenu === "protectTheNexusMenu"}
        setCurrentMenu={setCurrentMenu}
      />
      <div className="canvas-container">
        <canvas id="canvas" width={canvasWidth} height={canvasHeight}/>
      </div>
      <LoadImages />
    </div>
  );
}

class RunAnimation {
  constructor() {
    this.runAnimation = true;
  }
  startAnimation() {
    this.runAnimation = true;
  }
  stopAnimation() {
    this.runAnimation = false;
  }
  getRunAnimation() {
    return this.runAnimation;
  }
}
const runAnimation = new RunAnimation(); 

const showNextButton = (objects) => {
  const player = objects.getPlayer();
  if (player.stats.health <= 0) {
    return true;
  }
  let bool = true;
  objects.getNPCs().forEach((npc) => {
    if (npc.stats.health > 0 && npc.team !== player.team) {
      bool = false;
    }
  });
  return bool;
}

const startBattle = (userTeam, opponentTeam, background, runAnimation, setShowNextButton, timer, playerIndex) => {

  const animate = () => {
    if (runAnimation.getRunAnimation()) {
      requestAnimationFrame(animate);
    }
    else {
      return;
    }

    player.update(controls.getActions(), objects);
    camera.update(player, objects.getBackground());

    if (timer > 0) {
      timer--;
    }
    if (showNextButton(objects) || timer === 0) {
      setShowNextButton(true);
    }
    else {
      npcs.forEach((npc) => {npc.update()})
    }

    // Update ki blasts
    objects.getKiBlasts().forEach((obj) => {
      obj.update();
    });
    
    camera.drawScene(objects, timer);
  }

  // Initialize canvas, context, camera, controls
  const canvas = document.getElementById("canvas");
  const ctx = canvas.getContext("2d");
  const camera = new Camera(ctx, timer);
  const controls = new Controls();
  const soundsHandler = new SoundsHandler();

  runAnimation.startAnimation();
  setShowNextButton(false);

  // Initialize Players/Objects
  const DEFAULT_HEALTH = 500;

  const backgroundObj = new Background(0, 0, background);

  const playerObj = userTeam[playerIndex];
  const playerHealth = playerObj.health ? playerObj.health : DEFAULT_HEALTH;
  const player = new Character(100, 500, 54, 54, soundsHandler.getInstance(), Team.One, playerObj.name, playerHealth, playerObj.maxTransformations);

  const objects = new Objects(backgroundObj, player);
  const npcs = [];
  for (let i=0; i < userTeam.length; i++) {
    if (i === playerIndex) {
      continue;
    }
    const obj = userTeam[i].name;
    const health = userTeam[i].health ? userTeam[i].health : DEFAULT_HEALTH;
    if (obj !== "") {
      npcs.push(new NPC(100, 80*i, Team.One, obj, objects, health, userTeam[i].maxTransformations));
    }
  }

  for (let i=0; i < opponentTeam.length; i++) {
    const obj = opponentTeam[i].name;
    const health = opponentTeam[i].health ? opponentTeam[i].health : DEFAULT_HEALTH;
    if (obj !== "") {
      npcs.push(new NPC(backgroundObj.getDimensions()[2] - 100, 500, Team.Two, obj, objects, health, opponentTeam[i].maxTransformations));
    }
  }

  animate();
}

export default App;
