export const drawTimer = (ctx, timer, maxTimer) => {
    // Note timer 3000 ~ 50s
    // 1800 ~ 30s
    if (timer < 0) {
        return;
    }
    ctx.fillStyle = "#000000";
    ctx.fillRect(0, 0, 1500 * timer / maxTimer, 20);
    ctx.fillStyle = "#FF00FF";
    ctx.fillRect(1, 1, 1498 * timer / maxTimer, 18);
}
