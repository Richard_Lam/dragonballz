// @ts-ignore
import { Sounds } from "./Sounds.tsx";

export class SoundsHandler {
    
    sounds: Sounds

    constructor() {
        this.sounds = new Sounds();
    }
    getInstance() {
        return this.sounds;
    }
}