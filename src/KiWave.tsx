// @ts-ignore
import { Explosions } from "./Explosions.tsx";
// @ts-ignore
import { Objects } from "./Objects.tsx";
// @ts-ignore
import { Character } from "./player/Character.tsx";
// @ts-ignore
import { Direction } from "./player/Character.tsx";
// @ts-ignore
import { isRectsIntersect, Polygon, calculateKiWaveDamage, calculateKiBlastDamage } from "./Utils.tsx";

interface KiWaveComponent {
    x: number;
    y: number;
    width: number;
    height: number;
    baseWidth: number;
    sprite: HTMLElement | null;
}
enum KiWaveSize {
    SMALL,
    MEDIUM,
    LARGE,
    SUPER
}

const getKiWaveComponents = (kiWaveSize: KiWaveSize, x: number, y: number) => {
    if (kiWaveSize === KiWaveSize.SMALL) {
        const head = {
            x: x, y: y, width: 34, height: 30, baseWidth: 0,
            sprite: document.getElementById("small_blue_ki_wave_head")
        };
        const body = {
            x: x, y: y, width: 10, height: 30, baseWidth: 10,
            sprite: document.getElementById("small_blue_ki_wave_body")
        };
        const base = {
            x: x, y: y, width: 20, height: 30, baseWidth: 0,
            sprite: document.getElementById("small_blue_ki_wave_base")
        };
        return [head, body, base];
    }
    if (kiWaveSize === KiWaveSize.MEDIUM) {
        const head = {
            x: x, y: y, width: 40, height: 40, baseWidth: 0,
            sprite: document.getElementById("medium_blue_ki_wave_head")
        };
        const body = {
            x: x, y: y, width: 10, height: 40, baseWidth: 10,
            sprite: document.getElementById("medium_blue_ki_wave_body")
        };
        const base = {
            x: x, y: y, width: 20, height: 40, baseWidth: 0,
            sprite: document.getElementById("medium_blue_ki_wave_base")
        };
        return [head, body, base];
    }
    if (kiWaveSize === KiWaveSize.LARGE) {
        const head = {
            x: x, y: y, width: 60, height: 50, baseWidth: 0,
            sprite: document.getElementById("large_blue_ki_wave_head")
        };
        const body = {
            x: x, y: y, width: 10, height: 50, baseWidth: 10,
            sprite: document.getElementById("large_blue_ki_wave_body")
        };
        const base = {
            x: x, y: y, width: 30, height: 50, baseWidth: 0,
            sprite: document.getElementById("large_blue_ki_wave_base")
        };
        return [head, body, base];
    }
    const head = {
        x: x, y: y, width: 70, height: 60, baseWidth: 0,
        sprite: document.getElementById("super_blue_ki_wave_head")
    };
    const body = {
        x: x, y: y, width: 10, height: 60, baseWidth: 10,
        sprite: document.getElementById("super_blue_ki_wave_body")
    };
    const base = {
        x: x, y: y, width: 40, height: 60, baseWidth: 0,
        sprite: document.getElementById("super_blue_ki_wave_base")
    };
    return [head, body, base];
}

export class KiWave {

    kiWaveHead: KiWaveComponent;
    kiWaveBody: KiWaveComponent;
    kiWaveBase: KiWaveComponent;
    direction: Direction.left;
    objects: Objects;
    kiWaveSize: KiWaveSize;
    speed: number;
    timer: number;
    damage: number;
    owner: Character;
    grow: boolean;
    ownerSpecialAttack: number;

    constructor(objects: Objects, x, y, direction, owner, damage: number, ownerSpecialAttack: number) {
        this.direction = direction;
        this.objects = objects;
        objects.addKiBlast(this);

        this.kiWaveSize = calcKiWaveSize(damage);
        [this.kiWaveHead, this.kiWaveBody, this.kiWaveBase] = getKiWaveComponents(this.kiWaveSize, x, y);
        // const kiWaveBaseWidth = 20;
        // const baseX = direction === Direction.left ? x - kiWaveBaseWidth/3 : x;
        this.speed = 5;
        this.timer = 1;
        this.damage = damage;
        this.owner = owner;
        this.grow = true;
        this.ownerSpecialAttack = ownerSpecialAttack;

        // Removes ki blast
        setTimeout(() => {
            this.#removeSelf();
        }, 6000);
    }
    getDimensions() {
        // Changes depending on left or right
        return [this.kiWaveBody.x, this.kiWaveBody.y, this.kiWaveBody.width + (this.kiWaveHead.width - this.kiWaveBody.baseWidth), this.kiWaveHead.height];
    }
    update() {
        // Move Head
        if (this.direction === Direction.left) {
            this.kiWaveHead.x -= this.speed;

            this.kiWaveBody.x -= this.speed;
        }
        else {
            this.kiWaveHead.x += this.speed;
            if (!this.grow) {
                this.kiWaveBody.x += this.speed;
            }
        }
        if (this.grow) {
            this.kiWaveBody.width += this.speed;
        }
        for (const character of this.objects.getCharacters()) {
            const charPolygon = new Polygon(character.getDimensions());
            const kiBlastPolygon = new Polygon(this.getDimensions());
            if (this.owner !== character && character.getTeam() !== this.owner.getTeam() && isRectsIntersect(charPolygon, kiBlastPolygon)) {
                this.handleOnCollide(character);
            }
        }
        for (const kiBlast of this.objects.getKiBlasts()) {
            const kiBlastPolygon = new Polygon(kiBlast.getDimensions());
            const thisKiBlastPolygon = new Polygon(this.getDimensions());
            if (this.owner !== kiBlast.owner && this.owner.getTeam() !== kiBlast.owner.getTeam() && isRectsIntersect(kiBlastPolygon, thisKiBlastPolygon)) {
                this.handleCollideKiBlast(kiBlast);
            }
        }
        if (!this.owner.isFiringKiWave()) {
            this.grow = false;
        }
        this.timer++;
    }
    handleOnCollide(character) {
        this.objects.addExplosion(new Explosions(this.kiWaveHead.x, this.kiWaveHead.y, this.objects, this.damage));
        if (character.specialMoves.absorbEnergy) {
            character.addEnergy(this.damage);
        }
        else {
            character.removeHealth(calculateKiWaveDamage(this.damage, this.ownerSpecialAttack, character.stats.defense));
        }
        this.#removeSelf();
    }
    handleCollideKiBlast(kiBlast) {
        if (kiBlast.getClass() === 'kiblast') {
            const kiBlastDamage = calculateKiBlastDamage(kiBlast.damage, kiBlast.owner.stats.specialAttack, 1);
            const thisKiWaveDamage = calculateKiWaveDamage(this.damage, this.owner.stats.specialAttack, 1);
            if (kiBlastDamage < 0.5 * thisKiWaveDamage) {
                this.objects.removeKiBlast(kiBlast);
            }
        }
    }
    #removeSelf() {
        this.objects.removeKiBlast(this);
    }
    getClass() {
        return "kiwave";
    }
    draw(ctx, drawPos: any) {

        let imageXoffset = 0;
        let imageYoffset = 0;
        let xPosOffset = 0;
        let widthOffset = 0;
        if (this.direction === Direction.left) {
            imageXoffset = 1;
            xPosOffset = this.kiWaveHead.width;
            widthOffset = this.kiWaveHead.width;
        }
        else {
            xPosOffset = this.kiWaveBase.width;
            widthOffset = this.kiWaveBase.width;
        }
        if (Math.floor(this.timer / 5) % 2 === 0) {
            imageYoffset = 1;
        }

        const [x, y] = [drawPos[2].cameraX, drawPos[2].cameraY];
        // Draw ki wave body
        ctx.drawImage(
            this.kiWaveBody.sprite, 0, 0, this.kiWaveBody.baseWidth, this.kiWaveBody.height,
            this.kiWaveBody.x - x + xPosOffset, this.kiWaveBody.y - y, this.kiWaveBody.width - widthOffset, this.kiWaveBody.height
        );
        // Draw ki wave base
        if (this.grow) {
            ctx.drawImage(
                this.kiWaveBase.sprite, this.kiWaveBase.width*imageXoffset, this.kiWaveBase.height*imageYoffset, this.kiWaveBase.width, this.kiWaveBase.height,
                this.kiWaveBase.x - x, this.kiWaveBase.y - y, this.kiWaveBase.width, this.kiWaveBase.height
            );
        }
        // Draw ki wave head
        ctx.drawImage(
            this.kiWaveHead.sprite, this.kiWaveHead.width*imageXoffset, 0, this.kiWaveHead.width, this.kiWaveHead.height,
            this.kiWaveHead.x - x, this.kiWaveHead.y - y, this.kiWaveHead.width, this.kiWaveHead.height
        );
    }
}

const calcKiWaveSize = (damage: number) => {
    if (damage > 195) {
        return KiWaveSize.SUPER;
    }
    if (damage > 170) {
        return KiWaveSize.LARGE
    }
    if (damage > 130) {
        return KiWaveSize.MEDIUM;
    }
    return KiWaveSize.SMALL;
}
