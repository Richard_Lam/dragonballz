// @ts-ignore
import { isRectsIntersect, Polygon } from "./Utils.tsx";
// @ts-ignore
import { drawTimer } from "./drawTimer.tsx";

export class Camera {
    ctx: HTMLElement | null;
    x: number;
    y: number;
    width: number;
    height: number;
    maxTimer: number;
    constructor(ctx, timer) {
        this.ctx = ctx;
        this.x = 0;
        this.y = 0;
        this.width = 1500
        this.height = 600;
        this.maxTimer = timer;
    }
    update(player, background) {
        // Tracks player
        const [x, y] = player.getDimensions();
        this.x = x - (this.width / 2);
        this.y = y - (this.height / 2);

        const [bounds_x, bounds_y, bounds_x2, bounds_y2] = background.getBounds();
        if (this.x < bounds_x) {
            this.x = bounds_x;
        }
        else if (this.x + this.width > bounds_x2) {
            this.x = bounds_x2 - this.width;
        }
        if (this.y < bounds_y) {
            this.y = bounds_y;
        }
        else if (this.y + this.height > bounds_y2) {
            this.y = bounds_y2 - this.height;
        }
    }
    drawScene(objects, timer) {
        const allObjects = objects.getAll();
        const cameraPolygon = new Polygon(this.x, this.y, this.width, this.height);
        for (const obj of allObjects) {
            const [x, y] = obj.getDimensions();
            const objPolygon = new Polygon(obj.getDimensions());
            if (isRectsIntersect(cameraPolygon, objPolygon)) {
                const drawPos = [x - this.x, y - this.y, {cameraX: this.x, cameraY: this.y}];
                obj.draw(this.ctx, drawPos);
            }
        }
        // Draw Timer
        drawTimer(this.ctx, timer, this.maxTimer);
    }
}
