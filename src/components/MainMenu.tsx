import React from "react";
import "./MainMenu.scss";

interface MainMenuProps {
    visible: boolean;
    setCurrentMenu: React.Dispatch<React.SetStateAction<string>>;
}

export const MainMenu: React.FC<MainMenuProps> = ({ visible, setCurrentMenu }) => {
    return (visible ?
        <div id="MainMenu">
            <img src="./menus/mainMenu.gif" width={"100%"} height={"100%"} alt={"blah blah blah"}/>
            <div className="buttonContainer">
                <button className="button" onClick={() => setCurrentMenu("storyModeMenu")}> Story Mode </button>
                {/* <button className="button" onClick={() => setCurrentMenu("protectTheNexusMenu")}> Protect the Nexus </button> */}
            </div>
        </div>
    : null);
}
