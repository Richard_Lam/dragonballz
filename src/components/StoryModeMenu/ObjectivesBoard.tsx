import React from "react";
import "./ObjectivesBoard.scss";

interface ObjectivesBoardProps {
    objectives: string;
}

const ObjectivesBoard: React.FC<ObjectivesBoardProps> = ({ objectives }) => {

    return (
        <div className="objectives-board">
            <div className="position-relative center">
                <img
                    className="objectives-board-background"
                    src={"menus/objectives_board/goku.png"}
                    alt="objectives-board-background"
                />
                <div className="objectives">{objectives}</div>
            </div>
        </div>
    );
}

export default ObjectivesBoard;