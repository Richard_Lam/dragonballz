import React, { useEffect, useState } from "react";
import "./StoryModeMenu.scss";
import storyModeJson from "./storyMode.json";
// @ts-ignore
import { getProfilePicPath } from "../../Utils.tsx";
import statsJson from "../../player/stats.json";
// @ts-ignore
import { CharacterDisplays } from "./CharacterDisplays.tsx";
// @ts-ignore
import HeaderArrow from "./HeaderArrow.tsx";
// @ts-ignore
import ObjectivesBoard from "./ObjectivesBoard.tsx";

const storyModeObj = storyModeJson["storyMode"];

interface StoryModeMenuProps {
    startBattle: (userTeam, opponentTeam, background, currentMenu, setShowNextButton, timer, playerIndex) => void;
    runAnimation: any;
    setCurrentMenu: React.Dispatch<React.SetStateAction<string>>;
    visible: boolean;
    setShowNextButton: React.Dispatch<React.SetStateAction<boolean>>;
}

const padList = (lst) => {
    while (lst.length < 8) {
        lst.push({
            name: ""
        });
    }
    return lst;
}

export const StoryModeMenu: React.FC<StoryModeMenuProps> = ({ startBattle, runAnimation, setCurrentMenu, visible, setShowNextButton }) => {

    const sagas = Object.keys(storyModeObj);
    const [currentSagaIndex, setCurrentSagaIndex] = useState(0);
    const currentSaga = sagas[currentSagaIndex];
    const [currentSagaBattle, setCurrentSagaBattle] = useState(0);
    const [playerIndex, setPlayerIndex] = useState(0);
    
    const data = storyModeObj[currentSaga][currentSagaBattle];
    const userTeam = padList(data.userTeam);
    const opponentTeam = padList(data.opponentTeam);
    const background = data.background;
    const battleThumbnail = data["battle-thumbnail"];
    const title = data["title"];
    const objectives = data["objectives"] ? data["objectives"] : "";
    const timer = data["timer"] ? data["timer"] : -1;

    useEffect(() => {
        setPlayerIndex(0);
    }, [currentSagaBattle]);

    const playSound = (sound: string) => {
        const audio = new Audio(sound);
        audio.play();
    }
    const backToMainMenu = () => {
        setCurrentMenu("mainMenu");
    }

    const startFight = () => {
        setCurrentMenu("");
        startBattle(userTeam, opponentTeam, background, runAnimation, setShowNextButton, timer, playerIndex);
    }
    const handleClickCharacterSlot = (index) => {
        setPlayerIndex(index);
    }

    const decrementCurrentSaga = () => {
        setCurrentSagaIndex(currentSagaIndex - 1 >= 0 ? currentSagaIndex - 1 : 0);
        setCurrentSagaBattle(0);
    }
    const incrementCurrentSaga = () => {
        const nextCurrentSaga = currentSagaIndex + 1;
        if (nextCurrentSaga < sagas.length) {
            setCurrentSagaIndex(nextCurrentSaga);
        }
        setCurrentSagaBattle(0);
    }
    const decrementCurrentSagaBattle = () => {
        const previousSagaBattle = currentSagaBattle - 1;
        if (previousSagaBattle < 0) {
            decrementCurrentSaga();
            if (currentSagaIndex-1 >= 0) {
                const lastIndex = storyModeObj[sagas[currentSagaIndex-1]].length - 1;
                setCurrentSagaBattle(lastIndex);
            }
        }
        else {
            setCurrentSagaBattle(previousSagaBattle);
        }
        playSound("sounds/click_2.mp3");
    }
    const incrementCurrentSagaBattle = () => {
        const nextSagaBattle = currentSagaBattle+1;
        if (nextSagaBattle === storyModeObj[currentSaga].length) {
            if (currentSagaIndex === sagas.length - 1) {
                return;
            }
            incrementCurrentSaga();
            setCurrentSagaBattle(0);
            return;
        }
        setCurrentSagaBattle(nextSagaBattle);
        playSound("sounds/click_1.mp3");
    }

    return (visible ?
        <div id="storyModeMenu">
            <div className="left">
                <div className="battle-ground">
                    <img className="background" src={`./sprites/backgrounds/${background}.png`} alt={"background"}/>
                    <div className="empty-container-on-top-of-background">
                        <div className="position-relative">
                            <div className="header">
                                <div className="backButton" onClick={backToMainMenu}> BACK </div>
                                <div className="row">
                                    <HeaderArrow large={true} left={true} onClick={decrementCurrentSaga}/>
                                    <div className="sagaBox">{currentSaga}</div>
                                    <HeaderArrow large={true} left={false} onClick={incrementCurrentSaga}/>
                                </div>
                                <div className="row">
                                    <HeaderArrow large={false} left={true} onClick={decrementCurrentSagaBattle}/>
                                    <div className="currentBattle">{title}</div>
                                    <HeaderArrow large={false} left={false} onClick={incrementCurrentSagaBattle}/>
                                </div>
                            </div>
                            <ObjectivesBoard objectives={objectives}/>
                            <div className="characterDisplays">
                                <CharacterDisplays userTeam={userTeam} opponentTeam={opponentTeam}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="fight-button" onClick={startFight}> FIGHT </div>
            </div>
            <div className="divider" />
            <div className="right">
                <img src={`./story_mode_thumbnails/${battleThumbnail}.png`} className="battle-thumbnail" alt={"battle-thumbnail"}/>
                <div className="bottom-container">
                    <div className="all-character-slots">
                        <div className="character-slots">
                            {userTeam.map((user, index) => {
                                const userName = user.name;
                                if (userName === "") {
                                    return (
                                        <div className="character-slot" key={Math.random()*100}>
                                        </div>
                                    );
                                }
                                const characterProfilePicPath = getProfilePicPath(statsJson[userName]["image"]);
                                return (
                                    <div className="character-slot" key={Math.random()*100} onClick={() => handleClickCharacterSlot(index)}>
                                        <img src={characterProfilePicPath} alt="profile-pic"/>
                                        <div className="glow-on-hover cursor-pointer"/>
                                        {index === playerIndex ? <div className="selected-slot cursor-pointer"/> : null}
                                    </div>
                                );
                            })}
                        </div>
                        <div className="character-slots">
                            {opponentTeam.map((user) => {
                                const userName = user.name;
                                if (userName === "") {
                                    return (
                                        <div className="character-slot" key={Math.random()*100}>

                                        </div>
                                    );
                                }
                                const characterProfilePicPath = getProfilePicPath(statsJson[userName]["image"]);
                                return (
                                    <div className="character-slot" key={Math.random()*100}>
                                        <img src={characterProfilePicPath} alt="profile-pic"/>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                    <div className="bottom-labels">
                        <img src="./menus/user_team.png" alt={""}/>
                        <img src="./menus/opponent_team.png" alt={""}/>
                    </div>
                </div>
            </div>
        </div>
    : null);
}
