import React from "react";
import "./HeaderArrow.scss";

interface HeaderArrowProps {
    large: boolean;
    left: boolean;
    onClick?: () => void;
}

const HeaderArrow: React.FC<HeaderArrowProps> = ({ large, left, onClick }) => {

    const size = large ? "large" : "small";
    const direction = left ? "previous" : "forward";

    return (
        <img className={`header-arrow ${size}`} src={`./menus/${direction}_button.png`} alt="back" onClick={onClick}/>
    );
}

export default HeaderArrow;
