import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import statsJson from "../../player/stats.json";
import "./CharacterDisplays.scss";

interface CharacterDisplaysProps {
    userTeam: any[];
    opponentTeam: any[];
}

export const CharacterDisplays: React.FC<CharacterDisplaysProps> = ({userTeam, opponentTeam}) => {

    const [userIndex, setUserIndex] = useState(0);
    const [opponentIndex, setOpponentIndex] = useState(0);
    const [userTransformation, setUserTransformation] = useState(0);
    const [opponentTransformation, setOpponentTransformation] = useState(0);
    
    useEffect(() => {
        setUserIndex(0);
        setOpponentIndex(0);
        setUserTransformation(0);
        setOpponentTransformation(0);
    }, [userTeam, opponentTeam]);

    useEffect(() => {
        setTimeout(() => renderCharacters(userTeam, opponentTeam, userIndex, opponentIndex, userTransformation, opponentTransformation), 10);
    }, [userTeam, opponentTeam, userIndex, opponentIndex, userTransformation, opponentTransformation]);

    const nextUserIndex = () => {
        let currentUserIndex = userIndex;
        currentUserIndex += 1;
        if (currentUserIndex >= userTeam.filter(user => user.name !== "").length) {
            currentUserIndex = 0;
        }
        setUserIndex(currentUserIndex);
    }
    const transformUser = () => {
        let nextUserTransformation = userTransformation + 1;
        if (nextUserTransformation > userTeam[userIndex].maxTransformations) {
            nextUserTransformation = 0;
        }
        setUserTransformation(nextUserTransformation);
    }
    const transformOpponent = () => {
        let nextOpponentTransformation = opponentTransformation + 1;
        if (nextOpponentTransformation > opponentTeam[opponentIndex].maxTransformations) {
            nextOpponentTransformation = 0;
        }
        setOpponentTransformation(nextOpponentTransformation);
    }
    const nextOpponentIndex = () => {
        let currentOpponentIndex = opponentIndex;
        currentOpponentIndex += 1;
        if (currentOpponentIndex >= opponentTeam.filter(opponent => opponent.name !== "").length) {
            currentOpponentIndex = 0;
        }
        console.log(opponentIndex);
        setOpponentIndex(currentOpponentIndex);
    }

    return (
        <div id="characterDisplays">
            <img className="transformCharacterButton" onClick={transformUser} src="./menus/grey_up_arrow.png" alt="transformButton"/>
            <img className="nextCharacterButton" onClick={nextUserIndex} src="./menus/stats_next_user_button.png" alt="transformButton"/>
            <canvas id="characterDisplaysCanvas" width={300} height={100}/>
            <img className="transformCharacterButton" onClick={transformOpponent} src="./menus/grey_up_arrow.png" alt="transformButton"/>
            <img className="nextCharacterButton" onClick={nextOpponentIndex}
                src="./menus/stats_next_user_button.png"
                alt="nextCharButton"
            />
        </div>
    );
}

const getHighestStat = (userTeam, opponentTeam) => {
    let highestStat = 0;
    for (const user of [...userTeam, ...opponentTeam]) {
        const userName = user.name;
        if (userName === "") {
            continue;
        }
        let userStats = statsJson[userName];

        // Gets transformed version of user
        let totalTransformations = 0;
        const maxTransformations = user.maxTransformations !== undefined ? user.maxTransformations : 1000;
        while (userStats["transform"] && totalTransformations < maxTransformations) {
            userStats = statsJson[userStats["transform"]];
            totalTransformations++;
        }

        if (userStats.attack > highestStat) {
            highestStat = userStats.attack;
        }
        if (userStats.defense > highestStat) {
            highestStat = userStats.defense;
        }
        if (userStats.specialAttack > highestStat) {
            highestStat = userStats.specialAttack;
        }
        if (userStats.speed > highestStat) {
            highestStat = userStats.speed;
        }
    }
    return highestStat;
}

const getStatBarColor = (stat) => {
    if (stat < 100) {
        return "#FF0000";
    }
    if (stat < 300) {
        return "#FF7700"
    }
    if (stat < 600) {
        return "#FFFF00";
    }
}

const renderCharacters = (userTeam, opponentTeam, userIndex, opponentIndex, userTransformation, opponentTransformation) => {

    const drawBar = (x, y, width, height) => {
        ctx.fillRect(x, y - height, width, height);
    }

    const drawStatsBars = (x, ctx, stats) => {
        const barWidth = 10;
        const maxBarHeight = 80;
        for (let i=0; i < 4; i++) {
            const stat = [stats.attack, stats.defense, stats.specialAttack, stats.speed][i];
            const icon = document.getElementById(["attack_icon", "defense_icon", "special_attack_icon", "speed_icon"][i]);
            ctx.fillStyle = getStatBarColor(stat);
            drawBar(x + i*(barWidth + 5), 30 + userHeight, barWidth, (stat/highestStat)*maxBarHeight);
            ctx.drawImage(
                // @ts-ignore
                icon, 0, 0, icon?.width, icon?.height,
                // @ts-ignore
                x + i*(barWidth + 5), 30 + userHeight, icon?.width, icon?.height
            );
        }
    }

    let userStats = statsJson[userTeam[userIndex].name];
    let opponentStats = statsJson[opponentTeam[opponentIndex].name];

    while (userTransformation > 0) {
        userStats = userStats["transform"] ? statsJson[userStats["transform"]] : userStats;        
        userTransformation--;
    }
    while (opponentTransformation > 0) {
        opponentStats = opponentStats["transform"] ? statsJson[opponentStats["transform"]] : opponentStats;        
        opponentTransformation--;
    }

    const userImage = document.getElementById(userStats.image);
    const [userWidth, userHeight] = [userStats.width ? userStats.width : 54, userStats.height ? userStats.height : 54];

    const opponentImage = document.getElementById(opponentStats.image);
    const [opponentWidth, opponentHeight] = [opponentStats.width ? opponentStats.width : 54, opponentStats.height ? opponentStats.height : 54];

    const highestStat = getHighestStat(userTeam, opponentTeam);

    const canvas = document.getElementById("characterDisplaysCanvas");
    // @ts-ignore
    const ctx = canvas.getContext("2d");

    ctx.clearRect(0, 0, 300, 100);
    // ctx.fillRect(0, 0, 300, 100);

    const startingXpos = 22;

    const adjustedUserWidth = userWidth > 54 ? 54 : userWidth;
    const adjustedUserHeight = userHeight > 54 ? 54 : userHeight;
    ctx.drawImage(
        userImage, 0, 0, userWidth, userHeight,
        startingXpos, 30, adjustedUserWidth, adjustedUserHeight
    );
    const adjustedOpponentWidth = opponentWidth > 54 ? 54 : opponentWidth;
    const adjustedOpponentHeight = opponentHeight > 54 ? 54 : opponentHeight;
    ctx.drawImage(
        opponentImage, opponentWidth, 0, opponentWidth, opponentHeight,
        startingXpos + 200, 30 + (54 - adjustedOpponentHeight), adjustedOpponentWidth, adjustedOpponentHeight
    );
    drawStatsBars(startingXpos + 60, ctx, userStats);
    drawStatsBars(startingXpos + 60 + 55 + 25, ctx, opponentStats);
}
