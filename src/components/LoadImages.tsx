import React from "react";
import "./LoadImages.scss";

const LoadImages: React.FC = () => {

    const profilePics = [
        "adult_gohan_pfp", "mystic_gohan_pfp",
        "android_16_pfp", "nail_pfp",
        "android_17_pfp", "namek_ssj_goku_pfp",
        "android_18_pfp", "nappa_pfp",
        "android_19_pfp", "oozaru_vegeta_pfp",
        "android_saga_vegeta_pfp", "perfect_cell_pfp",
        "assj_vegeta_pfp", "piccolo_with_weights_pfp",
        "babidi_pfp", "pui_pui_pfp", "recoome_pfp",
        "burter_pfp", "saibaman_pfp",
        "cell_jr_pfp", "semi_perfect_cell_pfp",
        "cell_pfp", "spopovich_pfp",
        "chiaotzu_pfp",
        "cui_pfp", "ssj2_adult_gohan_pfp",
        "dabura_pfp", "ssj2_gohan_pfp",
        "dodoria_pfp", "ssj2_goku_pfp",
        "evil_buu_pfp", "ssj2_vegeta_gi_pfp",
        "fpssj_gohan_pfp", "ssj3_goku_pfp",
        "fpssj_goku_pfp", "ssj3_gotenks_pfp",
        "frieza_1_pfp", "ssj_adult_gohan_pfp",
        "frieza_2_pfp", "android_saga_ssj_future_trunks_pfp",
        "frieza_3_pfp", "ssj_goku_pfp",
        "frieza_4_pfp", "ssj_goten_pfp",
        "frieza_5_pfp", "ssj_gotenks_pfp",
        "android_saga_future_trunks_pfp", "ssj_kid_trunks_pfp",
        "dr_gero_pfp", "ssj_vegeta_gi_pfp",
        "ginyu_goku_pfp", "ssj_vegeta_pfp",
        "ginyu_pfp", "ssj_vegito_pfp",
        "goten_pfp", "super_buu_gohan_pfp",
        "gotenks_pfp", "super_buu_gotenks_pfp",
        "guldo_pfp", "super_buu_pfp",
        "hyperbolic_assj_trunks_pfp", "supreme_kai_pfp",
        "hyperbolic_ussj_trunks_pfp", "tien_pfp",
        "jeice_pfp", "transformed_zarbon_pfp",
        "kaioken_goku_pfp", "vegeta_gi_pfp",
        "kid_buu_pfp", "vegeta_pfp",
        "kid_gohan_pfp", "vegito_pfp",
        "kid_trunks_pfp", "videl_pfp",
        "king_cold_pfp", "yakon_pfp",
        "krillin_pfp", "yamcha_pfp",
        "majin_buu_pfp", "young_goku_pfp",
        "majin_vegeta_pfp", "zarbon_pfp",
        "mecha_frieza_pfp"
    ];

    return (
        <div className="load-images">
            <img id="majin_buu" src="./sprites/characters/buu/majin_buu.png" alt={"sprite"}/>
            <img id="cui" src="./sprites/characters/cui/cui.png" alt={"sprite"}/>
            <img id="dodoria" src="./sprites/characters/dodoria/dodoria.png" alt={"sprite"}/>
            <img id="zarbon" src="./sprites/characters/zarbon/zarbon.png" alt={"sprite"}/>
            <img id="transformed_zarbon" src="./sprites/characters/zarbon/transformed_zarbon.png" alt={"sprite"}/>

            <img id="guldo" src="./sprites/characters/ginyu_force/guldo.png" alt={"sprite"}/>
            <img id="recoome" src="./sprites/characters/ginyu_force/recoome.png" alt={"sprite"}/>
            <img id="burter" src="./sprites/characters/ginyu_force/burter.png" alt={"sprite"}/>
            <img id="jeice" src="./sprites/characters/ginyu_force/jeice.png" alt={"sprite"}/>
            <img id="ginyu" src="./sprites/characters/ginyu_force/ginyu.png" alt={"sprite"}/>
            <img id="ginyu_goku" src="./sprites/characters/ginyu_force/ginyu_goku.png" alt={"sprite"}/>

            <img id="frieza_1" src="./sprites/characters/frieza/frieza_1.png" alt={"sprite"}/>
            <img id="frieza_2" src="./sprites/characters/frieza/frieza_2.png" alt={"sprite"}/>
            <img id="frieza_3" src="./sprites/characters/frieza/frieza_3.png" alt={"sprite"}/>
            <img id="frieza_4" src="./sprites/characters/frieza/frieza_4.png" alt={"sprite"}/>
            <img id="frieza_5" src="./sprites/characters/frieza/frieza_5.png" alt={"sprite"}/>
            <img id="mecha_frieza" src="./sprites/characters/frieza/mecha_frieza.png" alt={"sprite"}/>
            <img id="king_cold" src="./sprites/characters/king_cold/king_cold.png" alt={"sprite"}/>

            <img id="cell_jr" src="./sprites/characters/cell_jr/cell_jr.png" alt={"sprite"}/>
            <img id="imperfect_cell" src="./sprites/characters/cell/imperfect_cell.png" alt={"sprite"}/>
            <img id="semi_perfect_cell" src="./sprites/characters/cell/semi_perfect_cell.png" alt={"sprite"}/>
            <img id="perfect_cell" src="./sprites/characters/cell/perfect_cell.png" alt={"sprite"}/>

            <img id="kid_gohan" src="./sprites/characters/gohan/kid_gohan.png" alt={"sprite"}/>
            <img id="mystic_gohan" src="./sprites/characters/gohan/mystic_gohan.png" alt={"sprite"}/>
            <img id="teen_gohan" src="./sprites/characters/gohan/teen_gohan.png" alt={"sprite"}/>
            <img id="fpssj_teen_gohan" src="./sprites/characters/gohan/fpssj_teen_gohan.png" alt={"sprite"}/>
            <img id="ssj2_teen_gohan" src="./sprites/characters/gohan/ssj2_teen_gohan.png" alt={"sprite"}/>

            <img id="base_goku" src="./sprites/characters/goku/base_goku.png" alt={"sprite"}/>
            <img id="kaioken_stage_1_goku" src="./sprites/characters/goku/kaioken_stage_1_goku.png" alt={"sprite"}/>
            <img id="kaioken_stage_2_goku" src="./sprites/characters/goku/kaioken_stage_2_goku.png" alt={"sprite"}/>
            <img id="namek_ssj_goku" src="./sprites/characters/goku/namek_ssj_goku.png" alt={"sprite"}/>
            <img id="ssj_goku" src="./sprites/characters/goku/ssj_goku.png" alt={"sprite"}/>
            <img id="fpssj_goku" src="./sprites/characters/goku/fpssj_goku.png" alt={"sprite"}/>
            <img id="ssj3_goku" src="./sprites/characters/goku/ssj3_goku.png" alt={"sprite"}/>

            <img id="krillin" src="./sprites/characters/krillin/krillin.png" alt={"sprite"}/>

            <img id="saibaman" src="./sprites/characters/saibaman/saibaman.png" alt={"sprite"}/>
            <img id="nappa" src="./sprites/characters/nappa/nappa.png" alt={"sprite"}/>

            <img id="piccolo_with_weights" src="./sprites/characters/piccolo/piccolo_with_weights.png" alt={"sprite"}/>
            <img id="piccolo_without_weights" src="./sprites/characters/piccolo/piccolo_without_weights.png" alt={"sprite"}/>
            <img id="nail" src="./sprites/characters/nail/nail.png" alt={"sprite"}/>
            <img id="dende" src="./sprites/characters/dende/dende.png" alt={"sprite"}/>

            <img id="raditz" src="./sprites/characters/raditz/raditz.png" alt={"sprite"}/>

            <img id="tien" src="./sprites/characters/tien/tien.png" alt={"sprite"}/>
            <img id="chiaotzu" src="./sprites/characters/chiaotzu/chiaotzu.png" alt={"sprite"}/>
            <img id="yamcha" src="./sprites/characters/yamcha/yamcha.png" alt={"sprite"}/>

            <img id="android_saga_future_trunks" src="./sprites/characters/future_trunks/android_saga_future_trunks.png" alt={"sprite"}/>
            <img id="android_saga_ssj_future_trunks" src="./sprites/characters/future_trunks/android_saga_ssj_future_trunks.png" alt={"sprite"}/>
            <img id="future_trunks" src="./sprites/characters/future_trunks/future_trunks.png" alt={"sprite"}/>
            <img id="ssj_future_trunks" src="./sprites/characters/future_trunks/ssj_future_trunks.png" alt={"sprite"}/>
            <img id="assj_future_trunks" src="./sprites/characters/future_trunks/assj_future_trunks.png" alt={"sprite"}/>
            <img id="ussj_future_trunks" src="./sprites/characters/future_trunks/ussj_future_trunks.png" alt={"sprite"}/>

            <img id="dr_gero" src="./sprites/characters/dr_gero/dr_gero.png" alt={"sprite"}/>
            <img id="android_19" src="./sprites/characters/android_19/android_19.png" alt={"sprite"}/>
            <img id="android_18" src="./sprites/characters/android_18/android_18.png" alt={"sprite"}/>
            <img id="android_17" src="./sprites/characters/android_17/android_17.png" alt={"sprite"}/>
            <img id="android_16" src="./sprites/characters/android_16/android_16.png" alt={"sprite"}/>

            <img id="vegeta" src="./sprites/characters/vegeta/vegeta.png" alt={"sprite"}/>
            <img id="great_ape_vegeta" src="./sprites/characters/vegeta/great_ape_vegeta.png" alt={"sprite"}/>
            <img id="ssj_vegeta" src="./sprites/characters/vegeta/ssj_vegeta.png" alt={"sprite"}/>
            <img id="assj_vegeta" src="./sprites/characters/vegeta/assj_vegeta.png" alt={"sprite"}/>

            <img id="ssj_vegito" src="./sprites/characters/vegito/ssj_vegito.png" alt={"sprite"}/>

            {profilePics.map((profile) =>
                <img id={profile} src={`./sprites/profile_pics/${profile}.png`} alt={"sprite"} key={Math.random()}/>
            )}


            <img id="namek" src="./sprites/backgrounds/namek.png" alt={"sprite"}/>
            <img id="hyperbolic_time_chamber" src="./sprites/backgrounds/hyperbolic_time_chamber.png" alt={"sprite"}/>
            <img id="kame_house" src="./sprites/backgrounds/kame_house.png" alt={"sprite"}/>
            <img id="raditz_area" src="./sprites/backgrounds/raditz_area.png" alt={"sprite"}/>
            <img id="wastelands" src="./sprites/backgrounds/wastelands.png" alt={"sprite"}/>
            <img id="exploding_namek" src="./sprites/backgrounds/exploding_namek.png" alt={"sprite"}/>
            <img id="friezas_ship" src="./sprites/backgrounds/friezas_ship.png" alt={"sprite"}/>
            <img id="city" src="./sprites/backgrounds/city.png" alt={"sprite"}/>
            <img id="cliff_side_road" src="./sprites/backgrounds/cliff_side_road.png" alt={"sprite"}/>
            <img id="cell_games" src="./sprites/backgrounds/cell_games.png" alt={"sprite"}/>
            <img id="mountains" src="./sprites/backgrounds/mountains.png" alt={"sprite"}/>
            <img id="capsule_corp_city" src="./sprites/backgrounds/capsule_corp_city.png" alt={"sprite"}/>

            <img id="small_ki_blast" src="./sprites/ki_blasts/small_ki_blast.png" alt={"sprite"}/>
            <img id="medium_ki_blast" src="./sprites/ki_blasts/medium_ki_blast.png" alt={"sprite"}/>
            <img id="large_ki_blast" src="./sprites/ki_blasts/large_ki_blast.png" alt={"sprite"}/>
            <img id="big_bang_attack" src="./sprites/ki_blasts/big_bang_attack.png" alt={"sprite"}/>

            <img id="blue_health_bar" src="./sprites/bars/blue_health_bar.png" alt={"sprite"}/>
            <img id="green_health_bar" src="./sprites/bars/green_health_bar.png" alt={"sprite"}/>
            <img id="yellow_health_bar" src="./sprites/bars/yellow_health_bar.png" alt={"sprite"}/>
            <img id="orange_health_bar" src="./sprites/bars/orange_health_bar.png" alt={"sprite"}/>
            <img id="red_health_bar" src="./sprites/bars/red_health_bar.png" alt={"sprite"}/>
            <img id="gray_health_bar" src="./sprites/bars/gray_health_bar.png" alt={"sprite"}/>

            <img id="blue_charging_bar" src="./sprites/bars/blue_charging_bar.png" alt={"sprite"}/>
            <img id="orange_charging_bar" src="./sprites/bars/orange_charging_bar.png" alt={"sprite"}/>

            <img id="small_explosions" src="./sprites/explosions/small_explosions.png" alt={"sprite"}/>
            <img id="medium_explosions" src="./sprites/explosions/medium_explosions.png" alt={"sprite"}/>
            <img id="large_explosions" src="./sprites/explosions/large_explosions.png" alt={"sprite"}/>
            <img id="super_large_explosions" src="./sprites/explosions/super_large_explosions.png" alt={"sprite"}/>
            <img id="giant_explosions" src="./sprites/explosions/giant_explosions.png" alt={"sprite"}/>
            <img id="super_giant_explosions" src="./sprites/explosions/super_giant_explosions.png" alt={"sprite"}/>

            <img id="green_charging_aura" src="./sprites/auras/green_aura.png" alt={"sprite"}/>
            <img id="yellow_charging_aura" src="./sprites/auras/yellow_aura.png" alt={"sprite"}/>
            <img id="red_charging_aura" src="./sprites/auras/red_aura.png" alt={"sprite"}/>
            <img id="blue_charging_aura" src="./sprites/auras/blue_aura.png" alt={"sprite"}/>
            <img id="white_charging_aura" src="./sprites/auras/white_aura.png" alt={"sprite"}/>
            <img id="purple_charging_aura" src="./sprites/auras/purple_aura.png" alt={"sprite"}/>
            <img id="pink_charging_aura" src="./sprites/auras/pink_aura.png" alt={"sprite"}/>

            <img id="small_blue_ki_wave_base" src="./sprites/ki_waves/small_blue_ki_wave_base.png" alt={"sprites"}/>
            <img id="small_blue_ki_wave_body" src="./sprites/ki_waves/small_blue_ki_wave_body.png" alt={"sprites"}/>
            <img id="small_blue_ki_wave_head" src="./sprites/ki_waves/small_blue_ki_wave_head.png" alt={"sprites"}/>

            <img id="medium_blue_ki_wave_base" src="./sprites/ki_waves/medium_blue_ki_wave_base.png" alt={"sprites"}/>
            <img id="medium_blue_ki_wave_body" src="./sprites/ki_waves/medium_blue_ki_wave_body.png" alt={"sprites"}/>
            <img id="medium_blue_ki_wave_head" src="./sprites/ki_waves/medium_blue_ki_wave_head.png" alt={"sprites"}/>

            <img id="large_blue_ki_wave_base" src="./sprites/ki_waves/large_blue_ki_wave_base.png" alt={"sprites"}/>
            <img id="large_blue_ki_wave_body" src="./sprites/ki_waves/large_blue_ki_wave_body.png" alt={"sprites"}/>
            <img id="large_blue_ki_wave_head" src="./sprites/ki_waves/large_blue_ki_wave_head.png" alt={"sprites"}/>

            <img id="super_blue_ki_wave_base" src="./sprites/ki_waves/super_blue_ki_wave_base.png" alt={"sprites"}/>
            <img id="super_blue_ki_wave_body" src="./sprites/ki_waves/super_blue_ki_wave_body.png" alt={"sprites"}/>
            <img id="super_blue_ki_wave_head" src="./sprites/ki_waves/super_blue_ki_wave_head.png" alt={"sprites"}/>

            <img id="super_yellow_ki_wave_base" src="./sprites/ki_waves/super_yellow_ki_wave_base.png" alt={"sprites"}/>
            <img id="super_yellow_ki_wave_body" src="./sprites/ki_waves/super_yellow_ki_wave_body.png" alt={"sprites"}/>
            <img id="super_yellow_ki_wave_head" src="./sprites/ki_waves/super_yellow_ki_wave_head.png" alt={"sprites"}/>

            <img id="thin_violet_ki_wave_base" src="./sprites/ki_waves/thin_violet_ki_wave_base.png" alt={"sprites"}/>
            <img id="thin_violet_ki_wave_body" src="./sprites/ki_waves/thin_violet_ki_wave_body.png" alt={"sprites"}/>
            <img id="thin_violet_ki_wave_head" src="./sprites/ki_waves/thin_violet_ki_wave_head.png" alt={"sprites"}/>

            <img id="special_beam_cannon_base" src="./sprites/ki_waves/special_beam_cannon_base.png" alt={"sprites"}/>
            <img id="special_beam_cannon_body" src="./sprites/ki_waves/special_beam_cannon_body.png" alt={"sprites"}/>
            <img id="special_beam_cannon_head" src="./sprites/ki_waves/special_beam_cannon_head.png" alt={"sprites"}/>

            <img id="attack_icon" src="./menus/icons/attack_icon.png" alt={"sprites"}/>
            <img id="defense_icon" src="./menus/icons/defense_icon.png" alt={"sprites"}/>
            <img id="special_attack_icon" src="./menus/icons/special_attack_icon.png" alt={"sprites"}/>
            <img id="speed_icon" src="./menus/icons/speed_icon.png" alt={"sprites"}/>
        </div>
    )
}

export default LoadImages;
