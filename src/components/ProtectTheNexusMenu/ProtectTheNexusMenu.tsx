import React from "react";
import "./ProtectTheNexusMenu.scss";
// @ts-ignore
import { Background } from '../.././Background.tsx';
// @ts-ignore
import { Camera } from '../.././Camera.tsx';
// @ts-ignore
import { Controls } from '../.././Controls.tsx';
// @ts-ignore
import { NPC } from '../.././player/NPC.tsx';
// @ts-ignore
import { SoundsHandler } from '../.././SoundsHandler.tsx';
// @ts-ignore
import { Objects } from "../.././Objects.tsx";
// @ts-ignore
import { Character } from "../.././player/Character.tsx";
// @ts-ignore
import { Team } from "../.././player/Character.tsx";
// @ts-ignore
import { Nexus } from "./Nexus.tsx";
// @ts-ignore
import { NexusAI } from "./NexusAI.tsx";
// @ts-ignore
import { NexusHealthDisplays } from "./NexusHealthDisplays.tsx";

interface ProtectTheNexusMenuProps {
    visible: boolean;
    setCurrentMenu: React.Dispatch<React.SetStateAction<string>>;
}

export const ProtectTheNexusMenu: React.FC<ProtectTheNexusMenuProps> = ({ visible, setCurrentMenu }) => {

    const onClick = () => {
        setCurrentMenu("");
        const opponentTeam: any = [
            {"name": "majin_buu"}
            // {"name" : "cell_saga_tien", defender: true},
            // {"name": "majin_buu", defender: true}
        ];
        startBattle(
            [
                {"name": "cell_saga_ssj2_teen_gohan"},
                {"name": "cell_saga_ssj2_teen_gohan"},
                {"name" : "cell_saga_tien", defender: true},
                {"name" : "cell_saga_ssj2_teen_gohan", defender: true},
                {"name" : "cell_saga_ssj2_teen_gohan", defender: true},
            ],
            opponentTeam,
            "capsule_corp_city",
            runAnimation, () => {}, -1, 0);
    }

    return (visible ?
        <div className="protectTheNexusMenu">
            <div onClick={onClick}> START </div>
        </div>
    : null);
}

class RunAnimation {

    runAnimation: boolean;

    constructor() {
      this.runAnimation = true;
    }
    startAnimation() {
      this.runAnimation = true;
    }
    stopAnimation() {
      this.runAnimation = false;
    }
    getRunAnimation() {
      return this.runAnimation;
    }
}
const runAnimation = new RunAnimation(); 

const showNextButton = (objects) => {
    const player = objects.getPlayer();
    let playerNexus = null
    for (const nexus of objects.getNexuses()) {
        if (nexus.getTeam() === player.getTeam()) {
            playerNexus = nexus;
        }
    }
    if (player.stats.health <= 0 && !playerNexus) {
        return true;
    }
    let bool = true;
    objects.getNPCs().forEach((npc) => {
        if (npc.stats.health > 0 && npc.team !== player.team) {
        bool = false;
        }
    });
    return bool;
}

const startBattle = (userTeam, opponentTeam, background, runAnimation, setShowNextButton, timer, playerIndex) => {

    const animate = () => {
        if (runAnimation.getRunAnimation()) {
            requestAnimationFrame(animate);
        }
        else {
            return;
        }

        player.update(controls.getActions(), objects);
        camera.update(player, objects.getBackground());
        objects.getNexuses().forEach((nexus) => {
            nexus.update();
        });

        if (timer > 0) {
            timer--;
        }
        if (showNextButton(objects) || timer === 0) {
            setShowNextButton(true);
        }
        else {
            npcs.forEach((npc) => {npc.update()})
        }

        // Update ki blasts
        objects.getKiBlasts().forEach((obj) => {
            obj.update();
        });
        
        camera.drawScene(objects, timer);
    }

    // Initialize canvas, context, camera, controls
    const canvas = document.getElementById("canvas");
    // @ts-ignore
    const ctx = canvas.getContext("2d");
    const camera = new Camera(ctx, timer);
    const controls = new Controls();
    const soundsHandler = new SoundsHandler();

    runAnimation.startAnimation();
    setShowNextButton(false);

    // Initialize Players/Objects
    const DEFAULT_HEALTH = 500;

    const backgroundObj = new Background(0, 0, background);

    const playerObj = userTeam[playerIndex];
    const playerHealth = playerObj.health ? playerObj.health : DEFAULT_HEALTH;
    const player = new Character(100, 500, 54, 54, soundsHandler.getInstance(), Team.One, playerObj.name, playerHealth, playerObj.maxTransformations);

    const objects = new Objects(backgroundObj, player);
    const npcs: NPC[] = [];
    for (let i=0; i < userTeam.length; i++) {
        if (i === playerIndex) {
            continue;
        }
        const obj = userTeam[i].name;
        const health = userTeam[i].health ? userTeam[i].health : DEFAULT_HEALTH;
        if (obj !== "") {
            const npc = new NPC(100, 80*i, Team.One, obj, objects, health, userTeam[i].maxTransformations);
            npc.ai = new NexusAI(npc.objects, npc.npc, {defender: userTeam[i].defender ? userTeam[i].defender : false});
            npcs.push(npc);
        }
    }

    for (let i=0; i < opponentTeam.length; i++) {
        const obj = opponentTeam[i].name;
        const health = opponentTeam[i].health ? opponentTeam[i].health : DEFAULT_HEALTH;
        if (obj !== "") {
            const npc = new NPC(backgroundObj.getDimensions()[2] - 100, 500, Team.Two, obj, objects, health, opponentTeam[i].maxTransformations);
            npc.ai = new NexusAI(npc.objects, npc.npc, {defender: opponentTeam[i].defender ? opponentTeam[i].defender : false});
            npcs.push(npc);
        }
    }

    const nexusOne = new Nexus(0, 250, Team.One, objects);
    const nexusTwo = new Nexus(backgroundObj.getDimensions()[2] - 100, 250, Team.Two, objects);
    objects.addNexus(nexusOne);
    objects.addNexus(nexusTwo);

    const nexusHealthDisplays = new NexusHealthDisplays(nexusOne, nexusTwo);
    objects.addExtras(nexusHealthDisplays);
    animate();
}
