// @ts-ignore
import { Nexus } from "./Nexus.tsx";

export class NexusHealthDisplays {
    
    x: number;
    y: number;
    healthBarLength: number;
    healthBarHeight: number;
    nexus1: Nexus;
    nexus2: Nexus;

    constructor(nexus1, nexus2) {
        const [ctxWidth] = [1500];
        this.healthBarLength = 200;
        this.healthBarHeight = 20;
        this.x = (ctxWidth / 2) - (this.healthBarLength / 2);
        this.y = 0;
        this.nexus1 = nexus1;
        this.nexus2 = nexus2;
    }
    getDimensions() {
        // this is to get the camera to draw this object
        return [0, 0, 10000, 10000];
    }
    draw(ctx: any, _: any) {
        ctx.fillStyle = "rgb(0, 0, 0, 200)";
        ctx.fillRect(this.x, this.y, this.healthBarLength, this.healthBarHeight);
        ctx.fillStyle = "rgb(0, 255, 0)";
        ctx.fillRect(this.x, this.y, (this.nexus1.health / 1000) * this.healthBarLength, this.healthBarHeight);

        ctx.fillStyle = "rgb(0, 0, 0, 100)";
        ctx.fillRect(this.x, this.y + this.healthBarHeight, this.healthBarLength, this.healthBarHeight);
        ctx.fillStyle = "rgb(255, 0, 0)";
        ctx.fillRect(this.x, this.y + this.healthBarHeight, (this.nexus2.health / 1000) * this.healthBarLength, this.healthBarHeight);
    }
}
