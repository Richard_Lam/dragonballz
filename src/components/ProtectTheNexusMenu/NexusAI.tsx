// @ts-ignore
import { AI } from "../../AI.tsx";
// @ts-ignore
import { Objects } from "./Objects.tsx";
// @ts-ignore
import { Character, Direction } from "../../player/Character.tsx";
// @ts-ignore
import { getDistance, randomInt } from "../../Utils.tsx";

export enum NexusAiType {
    defender,
    healer
}

export class NexusAI {
    
    objects: Objects
    npc: Character;
    punchTimer: number;
    normalAI: AI;

    nexusAiType: NexusAiType | null;
    defenderTarget: Character;
    chaseStates: {
        delay: number;
        wasAttacking: boolean;
    }
    tribeamStates: {
        isFiring: boolean;
    };

    constructor(objects, npc: Character, options={defender: null}) {
        this.objects = objects;
        this.npc = npc;
        this.punchTimer = 0;
        this.normalAI = new AI(objects, npc);
        this.nexusAiType = null;
        if (options.defender === true) {
            this.nexusAiType = NexusAiType.defender;
        }
        this.defenderTarget = null;
        this.chaseStates = {
            delay: 0,
            wasAttacking: false
        };
        this.tribeamStates = {
            isFiring: false
        }
    }
    getActions() {
        if (this.nexusAiType === NexusAiType.defender) {
            for (const nexus of this.objects.getNexuses()) {
                if (nexus.getTeam() === this.npc.getTeam()) {
                    return this.getDefenderActions(nexus);
                }
            }
        }
        for (const nexus of this.objects.getNexuses()) {
            if (nexus.getTeam() !== this.npc.getTeam()) {
                return this.attackNexus(nexus);
            }
        }
        return this.normalAI.getActions();
    }
    getDefenderActions(nexus) {
        this.defenderTarget = null;
        for (const character of this.objects.getCharacters()) {
            const characterCenter = character.getCenter();
            const nexusCenter = nexus.getCenter();
            if (character.getTeam() !== this.npc.getTeam() && getDistance(characterCenter, nexusCenter) < 400 && character.stats.health > 0) {
                this.defenderTarget = character;
            }
        }
        if (this.defenderTarget) {
            if (this.npc.specialMoves.triBeam) {
                return this.useTriBeam(this.defenderTarget, nexus);
            }
            return this.attackCharacter(this.defenderTarget);
        }
        // Charge as idle
        return {up: 0, down: 0, left: 0, right: 0, c: 1, x: 0, z: 0};
    }
    attackCharacter(target) {
        const REACTION_TIME = 20;

        const targetCenter = target.getCenter();
        const npcCenter = this.npc.getCenter();

        if (getDistance(npcCenter, targetCenter) > 20) {
            // If was attacking, add delay
            if (this.chaseStates.wasAttacking) {
                this.chaseStates.delay = randomInt(REACTION_TIME-2, REACTION_TIME+2);
                this.chaseStates.wasAttacking = false;
            }
            if (this.chaseStates.delay > 0) {
                this.chaseStates.delay--;
                return this.punching();
            }
            return this.chasePlayer(this.npc, target);
        }
        this.chaseStates.wasAttacking = true;
        return this.punching();
    }
    chasePlayer(npc: Character, target) {
        const targetCenter = target.getCenter();
        const npcCenter = npc.getCenter();

        const actions = {up: 0, down: 0, left: 0, right: 0, c: 1, x: 0, z: 0};
        if (Math.abs(targetCenter[0] - npcCenter[0]) > 10) {
            if (targetCenter[0] < npcCenter[0]) {
                actions.left = 1;
            }
            else {
                actions.right = 1;
            }
        }
        if (Math.abs(targetCenter[1] - npcCenter[1]) > 10) {
            if (targetCenter[1] < npcCenter[1]) {
                actions.up = 1;
            }
            else {
                actions.down = 1;
            }
        }
        return actions;
    }
    attackNexus(nexus) {

        const target = nexus;
        const targetCenter = target.getCenter();
        const npcCenter = this.npc.getCenter();

        if (getDistance(npcCenter, targetCenter) > 20) {
            return this.getToNexus(nexus)
        }
        return this.punching();
    }
    getToNexus(nexus) {
        const targetCenter = nexus.getCenter();
        const npcCenter = this.npc.getCenter();

        const actions = {up: 0, down: 0, left: 0, right: 0, c: 1, x: 0, z: 0};
        if (Math.abs(targetCenter[0] - npcCenter[0]) > 10) {
            if (targetCenter[0] < npcCenter[0]) {
                actions.left = 1;
            }
            else {
                actions.right = 1;
            }
        }
        if (Math.abs(targetCenter[1] - npcCenter[1]) > 10) {
            if (targetCenter[1] < npcCenter[1]) {
                actions.up = 1;
            }
            else {
                actions.down = 1;
            }
        }
        return actions;
    }
    punching() {
        // 5 punches per second
        const actions = {up: 0, down: 0, left: 0, right: 0, c: 0, x: 1, z: 0};
        this.punchTimer += 1;
        if (this.punchTimer < 20) {
            return actions;
        }
        actions.x = 0;
        this.punchTimer = 0;
        return actions;
    }
    useTriBeam(target, nexus) {
        // Charge enough
        if (this.npc.stats.energy < 200 && !this.tribeamStates.isFiring) {
            return {up: 0, down: 0, left: 0, right: 0, c: 1, x: 0, z: 0};
        }
        const targetXCenter = target.x + (target.width / 2);

        const actions = {up: 0, down: 0, left: 0, right: 0, c: 0, x: 0, z: 0};;

        if (this.npc.kiBlastDamage < 200) {
            if (this.npc.stats.energy === 0) {
                this.tribeamStates.isFiring = false;
                return actions;
            }
            else {
                actions.z = 1;
                this.tribeamStates.isFiring = true;
            }
        }

        // player is on left side of nexus
        const nexusCenter = nexus.getCenter();
        if (targetXCenter - nexusCenter < 0) {
            // player is on left of npc
            if (target.x < this.npc.x) {
                // tri beam ready to fire
                if (this.npc.kiBlastDamage === 200) {
                    // Align y's
                    const targetCenter = target.getCenter();
                    const npcCenter = this.npc.getCenter();
                    if (Math.abs(targetCenter[1] - npcCenter[1]) > 10) {
                        if (targetCenter[1] < npcCenter[1]) {
                            [actions.up, actions.z] = [1, 1];
                            return actions;
                        }
                        else {
                            [actions.down, actions.z] = [1, 1];
                            return actions;
                        }
                    }
                    // fire away
                    actions.z = 0;
                    this.tribeamStates.isFiring = false;
                    this.npc.direction = Direction.left;
                    return actions;
                }
                // keep charging tri beam
                return actions;
            }
            else {
                [actions.c, actions.right] = [1, 1]
                return actions;
            }
        }
        // player is on right side of nexus
        else {
            // player is on right of npc
            if (this.npc.x < target.x) {
                // tri beam ready to fire
                if (this.npc.kiBlastDamage === 200) {
                    // Align y's
                    const targetCenter = target.getCenter();
                    const npcCenter = this.npc.getCenter();
                    if (Math.abs(targetCenter[1] - npcCenter[1]) > 10) {
                        if (targetCenter[1] < npcCenter[1]) {
                            [actions.up, actions.z] = [1, 1];
                            return actions;
                        }
                        else {
                            [actions.down, actions.z] = [1, 1];
                            return actions;
                        }
                    }
                    // fire away
                    actions.z = 0;
                    this.tribeamStates.isFiring = false;
                    this.npc.direction = Direction.right;
                    return actions;
                }
                // keep charging tri beam
                return actions;
            }
            else {
                [actions.c, actions.left] = [1, 1]
                return actions;
            }
        }
    }
}
