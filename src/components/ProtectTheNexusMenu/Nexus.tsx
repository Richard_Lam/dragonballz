// @ts-ignore
import { Objects } from "../../Objects.tsx";
// @ts-ignore
import { Team, State, Character } from "../../player/Character.tsx";
// @ts-ignore
import {getDistance} from "../../Utils.tsx";

export class Nexus {

    x: number;
    y: number;
    width: number;
    height: number;
    health: number;
    maxHealth: number;
    team: Team;
    objects: Objects;
    revivingCharacters: Character[];

    healTimer: number;

    readonly reviveDelay = 10 * 1000;
    readonly healDelay = 10;

    constructor(x, y, team, objects) {
        this.x = x;
        this.y = y;
        this.width = 100;
        this.height = 100;
        this.health = 1000;
        this.maxHealth = 1000;
        this.team = team;
        this.objects = objects;
        this.revivingCharacters = [];
        this.healTimer = 0;
    }
    update() {
        for (const character of this.objects.getCharacters()) {
            // Revive dead teammates
            if (character.team === this.team && character.stats.health <= 0 && this.revivingCharacters.indexOf(character) < 0) {
                this.revivingCharacters.push(character);
                setTimeout(() => this.reviveCharacter(character), this.reviveDelay);
            }
        }
        // Heal teammates
        if (this.healTimer === this.healDelay) {
            for (const character of this.objects.getCharacters()) {
                if (character.getTeam() === this.team && getDistance(character.getCenter(), this.getCenter()) < 100) {
                    character.addHealth(5);
                }
            }
            this.healTimer = 0;
        }
        else {
            this.healTimer++;
        }
    }
    takeDamage(character) {
        this.health -= character.stats.attack / 300;
        if (this.health <= 0) {
            this.objects.removeNexus(this);
        }
    }
    reviveCharacter(character) {
        character.stats.health = 500;
        character.state = State.standing;
        character.x = this.x;
        character.y = this.y;
        this.revivingCharacters.splice(this.revivingCharacters.indexOf(character), 1);
    }
    getDimensions() {
        return [this.x, this.y, this.width, this.height];
    }
    getCenter() {
        return [this.x + this.width / 2, this.y + this.height / 2];
    }
    getTeam() {
        return this.team;
    }
    draw(ctx: any, drawPos: any) {
        ctx.fillStyle = "#FF0000";
        ctx.fillRect(drawPos[0], drawPos[1], this.width, this.height);
        ctx.fillStyle = "#00FF00";
        ctx.fillRect(drawPos[0], drawPos[1], this.width * this.health / this.maxHealth, 20);
    }
}