import React from "react";
import "./NextButton.scss";

interface NextButtonProps {
    setCurrentMenu: React.Dispatch<React.SetStateAction<string>>;
    setShowNextButton: any;
    runAnimation: any;
}

export const NextButton: React.FC<NextButtonProps> = ({ setCurrentMenu, setShowNextButton, runAnimation }) => {

    const handleClick = () => {
        setCurrentMenu("storyModeMenu");
        setShowNextButton(false);
        runAnimation.stopAnimation();
    }

    return (
        <div onClick={handleClick} id="nextButton">
            NEXT
        </div>
    );
}