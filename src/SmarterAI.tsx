// @ts-ignore
import { Objects } from "./Objects.tsx";
// @ts-ignore
import { Character } from "./Character.tsx";
// @ts-ignore
import { getDistance, randomInt } from "./Utils.tsx";
import { AI } from "./AI";

enum AIType {
    Normal,
    Typing
}

const getClosestEnemy = (self: Character, allCharacters: Character[]) => {
    let distance = 1_000_000;
    let target = null;
    for (let i=0; i < allCharacters.length; i++) {
        const currentCharacter = allCharacters[i];
        const currentDistance = getDistance(self.getCenter(), currentCharacter.getCenter());
        if (currentDistance < distance && self !== currentCharacter && self.team !== currentCharacter.team && currentCharacter.stats.health > 0) {
            target = currentCharacter;
            distance = currentDistance;
        }
    }
    return target;
}

export class SmarterAI {

    objects: Objects;
    npc: Character;
    target: Character;
    aiType: AIType;
    fireKiBlastsState: any;

    constructor(objects, npc) {
        this.objects = objects;
        this.npc = npc;
        this.target = null;
        this.aiType = AIType.Normal;
        this.fireKiBlastsState = {
            state: 0
        }
        setTimeout(() => {
            this.target = getClosestEnemy(npc, this.objects.getCharacters());
        }, 1);
    }
    getActions() {
        if (this.target === null) {
            return {up: 0, down: 0, left: 0, right: 0, c: 0, x: 0};
        }
        if (this.target.stats.health <= 0) {
            this.target = getClosestEnemy(this.npc, this.objects.getCharacters());
            if (this.target === null) {
                return {up: 0, down: 0, left: 0, right: 0, c: 0, x: 0};
            }
        }
        if (this.aiType === AIType.Normal) {
            return this.getNormalResponse();
        }
    }
    getNormalResponse() {
        if (this.target.kiBlastDamage > 50) {
            dodgePlayerChargingKiBlast
        }
        if (this.npc.stats.health < 100) {
            stayAwayAndBlast
        }
        if (this.target.stats.attack < this.npc.stats.attack) {
            punch
            rapidFireki
            fireKiBlastWaves
            fireKiBlastMaxPower
        }
    }
    fireKiBlasts(minimumEnergy, maximumEnergy) {
        // Decide what to fire
        // If player is near run
        // Charge enough energy
        // Start Firing
        // Align positions && face player
        // Release firing
    }
    rapidFireKiBlasts() {

    }
}