// @ts-ignore
import { Explosions } from "./Explosions.tsx";
// @ts-ignore
import { Objects } from "./Objects.tsx";
// @ts-ignore
import { Character, Direction } from "./player/Character.tsx";
// @ts-ignore
import { calculateKiBlastDamage } from "./Utils.tsx";
// @ts-ignore
import { isRectsIntersect, Polygon } from "./Utils.tsx";

const kiBlastMapper = (damage: number) => {
    if (damage < 30) {
        return {width: 10, height: 10, image: document.getElementById("small_ki_blast")};
    }
    if (damage < 70) {
        return {width: 20, height: 20, image: document.getElementById("medium_ki_blast")};
    }
    return {width: 30, height: 30, image: document.getElementById("large_ki_blast")};
}

export class KiBlast {

    objects: Objects;

    timer: number;
    direction: Direction
    x: number;
    y: number;
    width: number;
    height: number;
    speed: number;
    image: HTMLElement | null;
    damage: number;
    owner: Character;
    ownerSpecialAttack: number

    constructor(objects: Objects, x: number, y: number, direction, owner, damage, ownerSpecialAttack) {

        objects.addKiBlast(this);
        this.objects = objects;
        
        this.direction = direction;
        this.x = x;
        this.y = y;
        this.speed = 5;
        this.damage = damage;
        this.owner = owner;
        this.ownerSpecialAttack = ownerSpecialAttack;
        const kiBlastMap = kiBlastMapper(damage);
        [this.width, this.height, this.image] = [kiBlastMap.width, kiBlastMap.height, kiBlastMap.image];
        // Removes ki blast
        setTimeout(() => {
            this.removeSelf();
        }, 4000);
    }
    update() {
        this.timer--;
        if (this.direction === Direction.left) {
            this.x -= this.speed;
        }
        else {
            this.x += this.speed;
        }
        for (const character of this.objects.getCharacters()) {
            const charPolygon = new Polygon(character.getDimensions());
            const kiBlastPolygon = new Polygon(this.getDimensions());
            if (character !== this.owner && character.getTeam() !== this.owner.getTeam() && isRectsIntersect(charPolygon, kiBlastPolygon)) {
                this.objects.addExplosion(new Explosions(this.x, this.y, this.objects, this.damage));
                if (character.specialMoves.absorbEnergy) {
                    character.addEnergy(this.damage);
                }
                else {
                    character.removeHealth(calculateKiBlastDamage(this.damage, this.ownerSpecialAttack, character.stats.defense));
                }
                this.removeSelf();
            }
        }
    }
    getDimensions() {
        return [this.x, this.y, this.width, this.height];
    }
    getDamage() {
        return this.damage;
    }
    getOwner() {
        return this.owner;
    }
    setWidthAndHeight(width: number, height: number) {
        this.width = width;
        this.height = height;
    }
    setImage(image: HTMLElement | null) {
        this.image = image;
    }
    getClass() {
        return "kiblast";
    }
    draw(ctx: any, drawPos: any) {
        ctx.drawImage(
            this.image, 0, 0, this.width, this.height,
            drawPos[0], drawPos[1], this.width, this.height
        );
    }
    removeSelf() {
        this.objects.removeKiBlast(this);
    }
}