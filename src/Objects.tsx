// @ts-ignore
import { Background } from "./Background.tsx";
// @ts-ignore
import { Character } from "./player/Character.tsx";
// @ts-ignore
import { KiBlast } from "./KiBlast.tsx";
// @ts-ignore
import { KiWave } from "./KiWave.tsx";
// @ts-ignore
import { Explosions } from "./Explosions.tsx";
// @ts-ignore
import { Nexus } from "./components/ProtectTheNexusMenu/Nexus.tsx";

export class Objects {

    background: Background;
    player: Character;
    npcs: Character[];
    kiBlasts: (KiBlast | KiWave)[];
    explosions: Explosions[];
    nexuses: Nexus[];
    extras: any[];

    constructor(background, player) {
        this.background = background;
        this.player = player;
        this.npcs = [];
        this.kiBlasts = [];
        this.explosions = [];
        this.nexuses = [];
        this.extras = [];
    }
    addNPC(npc) {
        this.npcs.push(npc);
    }
    getPlayer() {
        return this.player;
    }
    getBackground() {
        return this.background;
    }
    getCharacters(): Character[] {
        return [this.player, ...this.npcs];
    }
    getNPCs() {
        return this.npcs;
    }
    addKiBlast(kiBlast: KiBlast) {
        this.kiBlasts.push(kiBlast);
    }
    addExplosion(explosions: Explosions) {
        this.explosions.push(explosions);
    }
    removeKiBlast(kiBlast: KiBlast) {
        const kiBlastIndex = this.kiBlasts.indexOf(kiBlast);
        if (kiBlastIndex >= 0) {
            this.kiBlasts.splice(kiBlastIndex, 1);
        }
    }
    removeExplosion(explosion: Explosions) {
        const explosionsIndex = this.explosions.indexOf(explosion);
        if (explosionsIndex >= 0) {
            this.explosions.splice(explosionsIndex, 1);
        }
    }
    getKiBlasts(): KiBlast[] {
        return this.kiBlasts;
    }
    getAll() {
        return [this.background, ...this.extras, ...this.nexuses, this.player, ...this.npcs, ...this.kiBlasts, ...this.explosions];
    }
    addExtras(extra) {
        this.extras.push(extra);
    }
    addNexus(nexus: any) {
        this.nexuses.push(nexus);
    }
    getNexuses() {
        return this.nexuses;
    }
    removeNexus(nexus: Nexus) {
        const nexusIndex = this.nexuses.indexOf(nexus);
        if (nexusIndex >= 0) {
            this.nexuses.splice(nexusIndex, 1);
        }
    }
}
